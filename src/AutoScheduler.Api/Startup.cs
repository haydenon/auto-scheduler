using System.Text.Json;
using AutoScheduler.Api.Graph;
using AutoScheduler.Api.Notion;
using AutoScheduler.Api.User;
using AutoScheduler.Data.Notion.Api.Client;
using AutoScheduler.Data.Notion.Api.Client.MockClient;
using GraphQL;
using GraphQL.Server;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AutoScheduler.Common.Models;
using AutoScheduler.Data.Google;
using AutoScheduler.Api.Google;

namespace AutoScheduler.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private readonly bool useMockData;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            useMockData = Configuration.GetValue<bool>("UseMockData", false);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<IDocumentWriter, GraphQL.SystemTextJson.DocumentWriter>();
            services.AddSingleton<DateTimeValueType>();

            services.AddScoped<ISchema, AutoSchedulerSchema>();
            services.AddScoped<AutoSchedulerQuery>();

            var rootDirectory = Configuration.GetValue<string>(WebHostDefaults.ContentRootKey);
            if (useMockData)
            {
                services.AddScoped<INotionApiClientFactory>(services =>
                {
                    var directory = $"{rootDirectory}/../../test_data";
                    return new MockClientFactory(directory);
                });
            }
            else
            {
                services.AddScoped<INotionApiClientFactory, NotionHttpApiClientFactory>();
            }

            services.AddScoped<IGoogleApiClientFactory, GoogleHttpClientFactory>();
            services.AddScoped<IGoogleTokenService, GoogleTokenService>();

            services.AddDatabase(Configuration, rootDirectory);
            services.AddNotionApis();
            services.AddServices();

            var mockAuthenticatedUser = Configuration["MockAuthenticatedUserId"];
            IUserContextBuilder builder = string.IsNullOrWhiteSpace(mockAuthenticatedUser)
                ? new UserContextBuilder()
                : new MockUserContextBuilder(mockAuthenticatedUser);

            services.AddGraphQL(options =>
            {
                options.EnableMetrics = true;
            })
            .AddErrorInfoProvider(opt => opt.ExposeExceptionStackTrace = true)
            .AddSystemTextJson(
                deserialiser => deserialiser.Converters.Add(new DateTimeValueConverter()),
                serialiser => serialiser.Converters.Add(new DateTimeValueConverter()))
            .AddUserContextBuilder(builder.GetUserContext);

            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            }); ;
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (!useMockData)
            {
                app.UseJwtContext();
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseGraphQLPlayground();
            }


            app.UseGraphQL<ISchema>();

            app.UseRouting();

            app.UseEndpoints(options =>
            {
                options.MapControllers();
            });
        }
    }
}
