using AutoScheduler.Core.Tasks;

namespace AutoScheduler.Api.Areas.Tasks;

public static class TaskMapperExtensions
{
    public static TaskTimeView MapToView(this TaskTime model)
        => new TaskTimeView(
            model.Start,
            model.End,
            model.IsRecurring
        );

    public static TaskView MapToView(this TaskItem model)
        => new TaskView(
            model.TaskId,
            model.TaskSource.ToString(),
            model.Title,
            model.Time.MapToView()
        );
}