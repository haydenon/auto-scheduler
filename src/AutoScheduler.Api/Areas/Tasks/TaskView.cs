using System;
using AutoScheduler.Common.Models;

namespace AutoScheduler.Api.Areas.Tasks;

public record TaskTimeView(
    IDateTimeValue Start,
    IDateTimeValue End,
    bool IsRecurring
);

public record TaskView(
    string TaskId,
    string TaskSource,
    string Title,
    TaskTimeView Time
);