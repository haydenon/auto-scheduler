using GraphQL.Types;

namespace AutoScheduler.Api.Areas.Tasks;

public class TaskType : ObjectGraphType<TaskView>
{
    public TaskType()
    {
        Name = "Task";

        Field(t => t.TaskId).Description("The id of the task.");
        Field(t => t.TaskSource).Description("The source of the task.");
        Field(t => t.Title).Description("The title of the task.");
        Field<NonNullGraphType<TaskTimeType>>(nameof(TaskView.Time), description: "The time of the task.");
    }
}

public class TaskTimeType : ObjectGraphType<TaskTimeView>
{
    public TaskTimeType()
    {
        Name = "TaskTime";

        Field(d => d.Start).Description("The start of the task's time.");
        Field(d => d.End).Description("The end of the task's time.");
        Field(d => d.IsRecurring).Description("Whether the task is recurring.");
    }
}
