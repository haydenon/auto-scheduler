
namespace AutoScheduler.Api.Areas.NotionKeys
{
    public record NotionKeyView(string ApiKey)
    {
        public string ApiKeyPreview => $"*****{ApiKey.Substring(ApiKey.Length - 3, 3)}";
    }
}