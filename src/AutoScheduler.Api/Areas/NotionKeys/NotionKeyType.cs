using GraphQL.Types;

namespace AutoScheduler.Api.Areas.NotionKeys
{
    public class NotionKeyType : ObjectGraphType<NotionKeyView>
    {
        public NotionKeyType()
        {
            Name = "NotionKey";

            Field(d => d.ApiKeyPreview).Description("A with preview of the API key with only some of the last characters shown.");
        }
    }
}