using System.Linq;
using AutoScheduler.Core.DatabaseConfigurations;

namespace AutoScheduler.Api.Areas.DatabaseConfigurations
{
    public static class DatabaseConfigurationMapperExtensions
    {
        public static StatusPropertyView MapToView(this StatusProperty model)
            => new StatusPropertyView(
                model.PropertyId,
                model.DefaultStatusId,
                model.InProgressStatusIds,
                model.FinishedStatusIds);

        public static TaskListDatabaseView MapToView(this NotionTaskList model)
            => new TaskListDatabaseView(model.DatabaseId, model.ScheduledDatePropertyId, model.StatusProperty.MapToView());

        public static DatabaseConfigurationView MapToView(this DatabaseConfiguration model)
            => new DatabaseConfigurationView(model.TaskListDatabases.Select(tld => tld.MapToView()).ToList());
    }
}