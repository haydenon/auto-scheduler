using System;
using System.Collections.Generic;

namespace AutoScheduler.Api.Areas.DatabaseConfigurations
{
    public record StatusPropertyView(
        string PropertyId,
        string DefaultStatusId,
        ICollection<string> InProgressStatusIds,
        ICollection<string> FinishedStatusIds
    );

    public record TaskListDatabaseView(
        Guid DatabaseId,
        string ScheduledDatePropertyId,
        StatusPropertyView StatusProperty
    );

    public record DatabaseConfigurationView(
        ICollection<TaskListDatabaseView> TaskListDatabases
    );
}