using GraphQL.Types;

namespace AutoScheduler.Api.Areas.DatabaseConfigurations
{

    public class DatabaseConfigurationType : ObjectGraphType<DatabaseConfigurationView>
    {
        public DatabaseConfigurationType()
        {
            Name = "DatabaseConfiguration";

            Field<ListGraphType<TaskListDatabaseType>>(nameof(DatabaseConfigurationView.TaskListDatabases), description: "Databases which maintain task lists.");
        }
    }

    public class TaskListDatabaseType : ObjectGraphType<TaskListDatabaseView>
    {
        public TaskListDatabaseType()
        {
            Name = "TaskListDatabase";

            Field(d => d.DatabaseId).Description("The id of the database.");
            Field(d => d.ScheduledDatePropertyId).Description("The id of the property for the scheduled task date.");
            Field<NonNullGraphType<StatusPropertyType>>(nameof(TaskListDatabaseView.StatusProperty), description: "The name of the database.");
        }
    }

    public class StatusPropertyType : ObjectGraphType<StatusPropertyView>
    {
        public StatusPropertyType()
        {
            Name = "StatusProperty";

            Field(sp => sp.PropertyId).Description("The id of the status property");
            Field(sp => sp.DefaultStatusId).Description("The id of default status for new tasks");
            Field(sp => sp.InProgressStatusIds).Description("The ids of statuses that indicate a task is in progress");
            Field(sp => sp.FinishedStatusIds).Description("The ids of statuses that indicate a task is finished");
        }
    }
}