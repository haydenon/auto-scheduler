using System.Threading.Tasks;
using AutoScheduler.Data.Google;

namespace AutoScheduler.Api.Google
{
    public class GoogleHttpClientFactory : IGoogleApiClientFactory
    {
        public IGoogleTokenService tokenService;

        public GoogleHttpClientFactory(IGoogleTokenService tokenService)
        {
            this.tokenService = tokenService;
        }

        public IGoogleClient GetClientForUser(string userId)
            => new GoogleClient(userId, tokenService);
    }
}