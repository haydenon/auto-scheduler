using System.Collections.Generic;
using AutoScheduler.Api.Areas.DatabaseConfigurations;
using AutoScheduler.Api.Areas.NotionKeys;
using AutoScheduler.Api.User;
using AutoScheduler.Data.NotionKeys;
using GraphQL;
using GraphQL.Types;
using LanguageExt;
using static LanguageExt.Prelude;
using AutoScheduler.Core.DatabaseConfigurations;
using AutoScheduler.Core.Databases;
using AutoScheduler.Api.Areas.Tasks;
using AutoScheduler.Core.Tasks;
using System.Linq;
using System;

namespace AutoScheduler.Api.Graph
{
    public class AutoSchedulerQuery : ObjectGraphType<object>
    {
        private readonly IDatabaseService databaseService;
        private readonly IDatabaseConfigurationService databaseConfigurationService;
        private readonly ITaskService taskService;
        private readonly INotionKeyRepository notionKeyRepository;

        public AutoSchedulerQuery(
            IDatabaseService databaseService,
            IDatabaseConfigurationService databaseConfigurationService,
            INotionKeyRepository notionKeyRepository,
            ITaskService taskService)
        {
            this.databaseService = databaseService;
            this.databaseConfigurationService = databaseConfigurationService;
            this.notionKeyRepository = notionKeyRepository;
            this.taskService = taskService;

            Name = "Query";

            FieldAsync<ListGraphType<TaskType>>(
                "tasks",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<DateTimeGraphType>>
                    {
                        Name = "timeMin",
                        Description = "The minimum time that tasks must end after."
                    },
                    new QueryArgument<NonNullGraphType<DateTimeGraphType>>
                    {
                        Name = "timeMax",
                        Description = "The maximum time that tasks must start before.",
                    }
                ),
                resolve: async (context) =>
                {
                    var timeMin = context.GetArgument<DateTime>("timeMin");
                    var timeMax = context.GetArgument<DateTime>("timeMax");
                    var userContext = context.UserContext as GraphQlUserContext;
                    if (userContext == null || userContext.UserId == null)
                    {
                        throw new ExecutionError("Invalid user id");
                    }

                    var filter = new TaskFilter(timeMin, timeMax);
                    var data = await taskService.GetTasksForUser(userContext.UserId, filter);
                    return data.Match(tasks => tasks.Select(t => t.MapToView()), err => throw new ExecutionError(err.Message));
                }
            );

            FieldAsync<DatabaseConfigurationType>(
                "databaseConfiguration",
                resolve: async (context) =>
                {
                    var userContext = context.UserContext as GraphQlUserContext;
                    if (userContext == null || userContext.UserId == null)
                    {
                        throw new ExecutionError("Invalid user id");
                    }

                    var data = await databaseConfigurationService.GetDatabaseConfigurationForUser(userContext.UserId);
                    return data.Match(dc => dc.MapToView(), err => throw new ExecutionError(err.Message));
                }
            );

            FieldAsync<NotionKeyType>(
                "notionKey",
                resolve: async (context) =>
                {
                    var userContext = context.UserContext as GraphQlUserContext;
                    if (userContext == null || userContext.UserId == null)
                    {
                        throw new ExecutionError("Invalid user id");
                    }

                    var data = await notionKeyRepository.GetNotionKeyForUser(userContext.UserId);
                    return data != null ? MapFromNotionKey(data) : throw new ExecutionError("Invalid Notion key");
                }
            );
        }

        private static NotionKeyView MapFromNotionKey(NotionKeyData data)
        {
            if (data == null || data.ApiKey == null)
            {
                throw new ExecutionError("Invalid Notion key configuration");
            }

            return new NotionKeyView(data.ApiKey);
        }
    }
}