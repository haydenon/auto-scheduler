using System;
using GraphQL;
using GraphQL.Types;
using GraphQL.Language.AST;
using AutoScheduler.Common.Models;
using System.Text.Json;

namespace AutoScheduler.Api.Graph
{
    public class DateTimeValueType : ScalarGraphType
    {
        private static readonly JsonSerializerOptions serializerOptions = new JsonSerializerOptions
        {
            Converters = { new DateTimeValueConverter() }
        };

        public override object? ParseLiteral(IValue value)
        {
            if (value is NullValue)
            {

                return null;
            }

            if (value is StringValue stringValue)
            {
                return ParseValue(stringValue.Value);
            }

            return ThrowLiteralConversionError(value);
        }

        public override object? ParseValue(object? value)
        {
            if (value == null)
                return null;

            if (value is string str)
            {
                return JsonSerializer.Deserialize<IDateTimeValue>(str, serializerOptions);
            }

            return ThrowValueConversionError(value);
        }

        public override object? Serialize(object? value)
        {
            if (value == null)
            {
                return null;
            }

            if (value is IDateTimeValue dateTimeValue)
            {
                return dateTimeValue.ToString();
            }

            return ThrowSerializationError(value);
        }
    }
}

