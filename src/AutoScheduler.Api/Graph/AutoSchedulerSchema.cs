using System;
using AutoScheduler.Common.Models;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace AutoScheduler.Api.Graph
{
    public class AutoSchedulerSchema : Schema
    {
        public AutoSchedulerSchema(IServiceProvider serviceProvider)
        {
            Query = serviceProvider.GetRequiredService<AutoSchedulerQuery>();
            RegisterTypeMapping(typeof(IDateTimeValue), typeof(DateTimeValueType));
        }
    }
}