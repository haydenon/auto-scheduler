using System.Linq;
using Microsoft.AspNetCore.Http;

namespace AutoScheduler.Api.User
{
    public class UserContextBuilder : IUserContextBuilder
    {
        private const string NameIdentifier = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";

        public GraphQlUserContext GetUserContext(HttpContext context)
        {
            if (context.User?.Identity != null && context.User.Identity.IsAuthenticated)
            {
                var userId = context.User.Claims.First(c => c.Type == NameIdentifier);
                return new GraphQlUserContext { UserId = userId.Value };
            }
            else
            {
                return new GraphQlUserContext { UserId = null };
            }
        }
    }
}