using Microsoft.AspNetCore.Http;

namespace AutoScheduler.Api.User
{
    public class MockUserContextBuilder : IUserContextBuilder
    {
        private readonly string mockUserId;

        public MockUserContextBuilder(string mockUserId)
        {
            this.mockUserId = mockUserId;
        }

        public GraphQlUserContext GetUserContext(HttpContext context)
            => new GraphQlUserContext { UserId = mockUserId };
    }
}