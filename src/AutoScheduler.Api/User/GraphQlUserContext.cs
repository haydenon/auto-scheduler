using System.Collections.Generic;

namespace AutoScheduler.Api.User
{
    public class GraphQlUserContext : Dictionary<string, object>
    {
        public string? UserId { get; set; }
    }
}