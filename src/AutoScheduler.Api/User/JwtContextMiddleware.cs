using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
namespace AutoScheduler.Api.User
{
    public class JwtContextMiddleware
    {
        private const string GoogleDomain = "accounts.google.com";
        private const string NameIdentifier = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";

        private static JsonSerializerOptions serializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };

        private readonly RequestDelegate _next;

        public JwtContextMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        private static Lazy<OpenIdConnectConfiguration> OpenIdConfig = new Lazy<OpenIdConnectConfiguration>(
            () =>
            {
                var configurationManager =
                    new ConfigurationManager<OpenIdConnectConfiguration>(
                        $"https://{GoogleDomain}/.well-known/openid-configuration",
                        new OpenIdConnectConfigurationRetriever());
                return configurationManager.GetConfigurationAsync(CancellationToken.None).GetAwaiter().GetResult();
            }
        );

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.StartsWithSegments("/ui/playground"))
            {
                await _next.Invoke(context);
                return;
            }

            var configuration = context.RequestServices.GetRequiredService<IConfiguration>();
            var problemDetailFactory = context.RequestServices.GetRequiredService<ProblemDetailsFactory>();
            var audience = configuration.GetSection("OAuth").GetValue<string>("ClientId");

            var (success, userClaims) = GetUserClaims(context, audience);
            if (!success || userClaims == null)
            {
                var problem = problemDetailFactory.CreateProblemDetails(
                                context,
                                statusCode: (int)HttpStatusCode.Unauthorized);
                var result = JsonSerializer.Serialize(problem, serializerOptions);
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await context.Response.WriteAsync(result);
                return;
            }

            context.User = userClaims;
            await _next.Invoke(context);
        }

        private (bool, ClaimsPrincipal?) GetUserClaims(HttpContext context, string audience)
        {
            try
            {
                var openIdConfig = OpenIdConfig.Value;
                var validationParameters = new TokenValidationParameters
                {
                    ValidIssuer = $"https://{GoogleDomain}",
                    ValidAudiences = new[] { audience },
                    IssuerSigningKeys = openIdConfig.SigningKeys
                };

                var authHeader = context.Request.Headers["Authorization"];
                if (authHeader.Count > 1 || !authHeader[0].StartsWith("Bearer", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new ArgumentException("Invalid authorization header");
                }

                var token = authHeader[0].Substring("Bearer ".Length);

                SecurityToken validatedToken;
                var handler = new JwtSecurityTokenHandler();
                var user = handler.ValidateToken(token, validationParameters, out validatedToken);

                return (true, user);
            }
            catch
            {
                return (false, null);
            }
        }
    }

    public static class JwtContextMiddlewareExtensions
    {
        public static IApplicationBuilder UseJwtContext(this IApplicationBuilder builder)
            => builder.UseMiddleware<JwtContextMiddleware>();
    }
}