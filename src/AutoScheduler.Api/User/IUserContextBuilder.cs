using Microsoft.AspNetCore.Http;

namespace AutoScheduler.Api.User
{
    public interface IUserContextBuilder
    {
        GraphQlUserContext GetUserContext(HttpContext context);
    }
}