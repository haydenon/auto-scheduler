using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using AutoScheduler.Common;
using AutoScheduler.Data.DatabaseConfiguration;
using AutoScheduler.Data.GoogleOAuth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AutoScheduler.Api.User;
public record AuthResponse(bool HasConsented);

public class TokenRequest
{
    public string? Code { get; init; }
}

public record TokenResponse(bool WasSuccessful);

[Route("auth")]
[Controller]
public class UserController : Controller
{
    private readonly IGoogleOAuthRepository googleOAuthRepository;
    private readonly IDatabaseConfigurationRepository databaseConfigurationRepository;
    private readonly IConfiguration configuration;
    private static readonly HttpClient client = new HttpClient();
    private static JsonSerializerOptions tokenSerializerOptions = new JsonSerializerOptions
    {
        PropertyNameCaseInsensitive = true,
        PropertyNamingPolicy = SnakeCaseNamingPolicy.Instance
    };

    private const string NameIdentifier = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";

    public UserController(
        IGoogleOAuthRepository googleOAuthRepository,
        IDatabaseConfigurationRepository databaseConfigurationRepository,
        IConfiguration configuration)
    {
        this.googleOAuthRepository = googleOAuthRepository;
        this.databaseConfigurationRepository = databaseConfigurationRepository;
        this.configuration = configuration;
    }

    [Route("details")]
    public async Task<ActionResult<AuthResponse>> GetAuthDetails()
    {
        if (HttpContext.User?.Identity == null || !HttpContext.User.Identity.IsAuthenticated)
        {
            return Problem("Unauthorized", statusCode: (int)HttpStatusCode.Unauthorized);
        }

        var userId = HttpContext.User.Claims.First(c => c.Type == NameIdentifier).Value;
        var googleTask = googleOAuthRepository.GetGoogleOAuthForUser(userId);
        var databaseConfigTask = databaseConfigurationRepository.GetDatabaseConfigurationForUser(userId);
        var googleOAuthData = await googleTask;
        var databaseConfiguration = await databaseConfigTask;
        if (databaseConfiguration == null)
        {
            return Problem("Forbidden", statusCode: (int)HttpStatusCode.Forbidden);
        }

        var hasConsented = googleOAuthData?.RefreshToken != null;
        return new AuthResponse(hasConsented);
    }

    [Route("token")]
    [HttpPost]
    public async Task<ActionResult> PostToken([FromBody] TokenRequest request)
    {
        if (HttpContext.User?.Identity == null || !HttpContext.User.Identity.IsAuthenticated)
        {
            return Problem("Unauthorized", statusCode: (int)HttpStatusCode.Unauthorized);
        }
        var userId = HttpContext.User.Claims.First(c => c.Type == NameIdentifier).Value;

        var code = request.Code;
        if (string.IsNullOrWhiteSpace(code) || code.Length < 40)
        {
            return Problem("Invalid code", statusCode: (int)HttpStatusCode.BadRequest);
        }

        var oauthSection = configuration.GetSection("OAuth");
        var content = new FormUrlEncodedContent(
            new List<KeyValuePair<string, string>>
            {
                    new KeyValuePair<string, string>("client_id", oauthSection.GetValue<string>("ClientId")),
                    new KeyValuePair<string, string>("client_secret", oauthSection.GetValue<string>("ClientSecret")),
                    new KeyValuePair<string, string>("code", code),
                    new KeyValuePair<string, string>("grant_type", "authorization_code"),
                    new KeyValuePair<string, string>("redirect_uri", "postmessage")
            }
        );

        content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
        content.Headers.ContentType.CharSet = "UTF-8";
        client.DefaultRequestHeaders.ExpectContinue = false;
        var response = await client.PostAsync(new Uri(oauthSection.GetValue<string>("Endpoint")), content);
        if (!response.IsSuccessStatusCode)
        {
            return Problem("Auth request failed", statusCode: (int)HttpStatusCode.BadRequest);
        }

        var respContent = await response.Content.ReadAsStringAsync();
        try
        {
            var tokenResponse = JsonSerializer.Deserialize<GoogleTokenResponse>(respContent, tokenSerializerOptions);
            var refreshToken = tokenResponse?.RefreshToken;
            if (refreshToken == null || string.IsNullOrWhiteSpace(refreshToken))
            {
                return Ok(new TokenResponse(false));
            }

            await googleOAuthRepository.SetRefreshTokenForUser(userId, refreshToken);
            return Ok(new TokenResponse(true));
        }
        catch
        {
            return Problem("Auth request failed", statusCode: (int)HttpStatusCode.BadRequest);
        }
    }


    public class GoogleTokenResponse
    {
        public string? AccessToken { get; set; }
        public long ExpiresIn { get; set; }
        public string? RefreshToken { get; set; }
        public string? Scope { get; set; }
        public string? TokenType { get; set; }
        public string? IdToken { get; set; }
    }
}