using System;
using System.Linq;
using System.Threading.Tasks;
using AutoScheduler.Data.NotionKeys;
using AutoScheduler.Data.Notion.Api;
using AutoScheduler.Data.Notion.Api.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace AutoScheduler.Api.Notion
{
    public class NotionHttpApiClientFactory : INotionApiClientFactory
    {
        private const string NameIdentifier = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";

        private const string NotionConfigSection = "Notion";
        private const string NotionApiVersion = "ApiVersion";

        private readonly IConfiguration configuration;
        private readonly INotionKeyRepository notionKeyRepository;
        private readonly IHttpContextAccessor httpContextAccessor;

        public NotionHttpApiClientFactory(
            INotionKeyRepository notionKeyRepository,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration)
        {
            this.configuration = configuration;
            this.notionKeyRepository = notionKeyRepository;
            this.httpContextAccessor = httpContextAccessor;
        }

        public Task<INotionClient> GetClientForCurrentUser()
        {
            var context = httpContextAccessor.HttpContext;
            if (context?.User?.Identity != null)
            {
                var userId = context.User.Claims.First(c => c.Type == NameIdentifier);
                return GetClientForUser(userId.Value);
            }
            else
            {
                throw new UnauthorizedAccessException();
            }
        }

        public Task<INotionClient> GetClientForUser(string userId)
        {
            var userContextFactory = new NotionContextFactory(
                notionKeyRepository,
                configuration
            );
            return userContextFactory.GetClientForUser(userId);
        }
    }
}