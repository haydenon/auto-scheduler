using AutoScheduler.Data.Notion.Api.Client;
using AutoScheduler.Data.Notion.Api.Client.MockClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AutoScheduler.Jobs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            useMockData = Configuration.GetValue<bool>("UseMockData", false);
        }

        public IConfiguration Configuration { get; }
        private readonly bool useMockData;


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddJwtAuthentication(Configuration);

            var rootDirectory = Configuration.GetValue<string>(WebHostDefaults.ContentRootKey);
            if (useMockData)
            {
                services.AddScoped<INotionApiClientFactory>(services =>
                {
                    var directory = $"{rootDirectory}/../../test_data";
                    return new MockClientFactory(directory);
                });
            }
            else
            {
                services.AddScoped<INotionApiClientFactory, NotionContextFactory>();
            }

            services.AddDatabase(Configuration, rootDirectory);
            services.AddNotionApis();
            services.AddServices();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                var controllersBuilder = endpoints.MapControllers();
                if (!Configuration.GetValue<bool>("AllowUnauthenticated", false))
                {
                    controllersBuilder.RequireAuthorization();
                }
            });
        }
    }
}
