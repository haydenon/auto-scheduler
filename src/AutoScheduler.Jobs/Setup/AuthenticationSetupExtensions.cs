using System.Threading;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class AuthenticationSetupExtensions
    {
        public static void AddJwtAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var useMockData = configuration.GetValue<bool>("UseMockData", false);
            if (useMockData)
            {
                return;
            }

            const string GoogleDomain = "accounts.google.com";
            var Audience = configuration.GetSection("OAuth").GetValue<string>("Audience");
            var configurationManager =
                new ConfigurationManager<OpenIdConnectConfiguration>(
                    $"https://{GoogleDomain}/.well-known/openid-configuration",
                    new OpenIdConnectConfigurationRetriever());
            var openIdConfig = configurationManager.GetConfigurationAsync(CancellationToken.None).GetAwaiter().GetResult();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                var validationParameters = new TokenValidationParameters
                {
                    ValidIssuer = GoogleDomain,
                    ValidAudiences = new[] { Audience },
                    IssuerSigningKeys = openIdConfig.SigningKeys
                };
            });
        }
    }
}
