using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace AutoScheduler.Jobs
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseKestrel(options =>
                    {
                        var portVar = Environment.GetEnvironmentVariable("PORT");
                        if (!string.IsNullOrWhiteSpace(portVar) && int.TryParse(portVar, out var port))
                        {
                            options.ListenAnyIP(port);
                        }
                    });
                });
    }
}
