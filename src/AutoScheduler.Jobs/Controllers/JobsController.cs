﻿using System.Threading.Tasks;
using AutoScheduler.Core.DatabaseConfigurations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AutoScheduler.Jobs.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JobsController : ControllerBase
    {
        private readonly IDatabaseConfigurationService databaseConfigurationService;
        private readonly ILogger<JobsController> logger;

        public JobsController(
            IDatabaseConfigurationService databaseConfigurationService,
            ILogger<JobsController> logger)
        {
            this.databaseConfigurationService = databaseConfigurationService;
            this.logger = logger;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<JsonResult> Run()
        {
            await foreach (var database in databaseConfigurationService.GetAllDatabaseConfigurations())
            {
                this.logger.LogInformation($"[user '{database.UserId}'] Found {database.TaskListDatabases.Count} task databases");
                var missedTaskCount = 0;
                foreach (var taskList in database.TaskListDatabases)
                {
                }

                this.logger.LogInformation($"[user '{database.UserId}'] Updated {missedTaskCount} missed tasks for user");
            }

            return new JsonResult("Ok");
        }
    }
}
