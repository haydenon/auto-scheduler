using System;
using System.Collections.Generic;
using System.Linq;

namespace AutoScheduler.Common.Errors
{
    public record AggregateError(IEnumerable<IError> Errors) : IError
    {
        public string Message => Errors.Count() > 1
            ? $"Encountered multiple errors:\n{string.Join("\n-", Errors.Select(e => e.Message))}"
            : Errors.First().Message;
    }
}