namespace AutoScheduler.Common.Errors
{
    public record GoogleApisNotConfiguredError() : IError
    {
        public string Message => "You have not set up Google API access.";
    }
}