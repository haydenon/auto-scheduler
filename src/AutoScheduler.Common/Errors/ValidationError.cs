namespace AutoScheduler.Common.Errors
{
    public record ValidationError(string Message) : IError;
}