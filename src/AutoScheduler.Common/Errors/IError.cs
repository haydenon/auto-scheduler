namespace AutoScheduler.Common.Errors
{
    public interface IError
    {
        string Message { get; }
    }
}