using System.Net;

namespace AutoScheduler.Common.Errors
{
    public record ApiError(string Message, HttpStatusCode? Code) : IError;
}