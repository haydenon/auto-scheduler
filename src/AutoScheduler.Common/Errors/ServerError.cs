namespace AutoScheduler.Common.Errors
{
    public record ServerError(string Message) : IError;
}