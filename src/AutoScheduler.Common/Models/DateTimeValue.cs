using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace AutoScheduler.Common.Models
{
    public interface IDateTimeValue
    {
        DateTime GetUtcDateTime(TimeZoneInfo userTimeZone);
    }

    public record DateTimeValue(DateTime Value) : IDateTimeValue
    {
        public DateTime GetUtcDateTime(TimeZoneInfo userTimeZone)
            => Value;

        public override string ToString()
            => Value.ToString("o");
    }

    public record DateOnlyValue(DateOnly Value) : IDateTimeValue
    {
        private static readonly JsonSerializerOptions serializerOptions = new JsonSerializerOptions
        {
            Converters = { new DateTimeValueConverter() }
        };

        private DateTime GetDateInUtc(TimeZoneInfo timeZone)
        {
            var dateTimeUnspec = Value.ToDateTime(TimeOnly.MinValue, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeToUtc(dateTimeUnspec, timeZone);
        }

        public DateTime GetUtcDateTime(TimeZoneInfo userTimeZone)
            => GetDateInUtc(userTimeZone);

        public override string ToString()
            => Value.ToString("yyyy-MM-dd");
    }

    public class DateTimeValueConverter : JsonConverter<IDateTimeValue>
    {
        private static readonly Regex DatePattern = new Regex("^([0-9]{4})-?(1[0-2]|0[1-9])-?(3[01]|0[1-9]|[12][0-9])$", RegexOptions.Compiled);
        public override IDateTimeValue? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var value = reader.GetString();
            if (value == null)
            {
                return null;
            }
            if (DatePattern.IsMatch(value))
            {
                var date = DateOnly.Parse(value);
                return new DateOnlyValue(date);
            }

            var dateTime = DateTimeOffset.Parse(value);
            return new DateTimeValue(dateTime.UtcDateTime);
        }

        public override void Write(Utf8JsonWriter writer, IDateTimeValue value, JsonSerializerOptions options)
        {
            switch (value)
            {
                case DateTimeValue { Value: DateTime dateTime }:
                    JsonSerializer.Serialize(writer, dateTime, options);
                    break;
                case DateOnlyValue { Value: DateOnly dateOnly }:
                    writer.WriteStringValue(dateOnly.ToString("yyyy-MM-dd"));
                    break;
                default:
                    throw new NotImplementedException($"No implementation for serialising {value.GetType().Name}");
            }
        }

    }
}