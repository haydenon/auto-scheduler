using System;

namespace AutoScheduler;

public static class Prelude
{
    public static Func<T, bool> Not<T>(Func<T, bool> f) => value => !f(value);
}