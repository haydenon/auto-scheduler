using System;
using System.Collections.Generic;

namespace AutoScheduler.Common.Handlers
{
    public interface IQueuePublisherService<T>
    {
        void Publish(T value);
    }

    public interface IQueueSubscriptionService<T>
    {
        void Subscribe(Action<T> subscription);
    }

    public interface IHandlerQueueService<T> : IQueuePublisherService<T>, IQueueSubscriptionService<T>
    {
    }

    public class HandlerQueue<T> : IHandlerQueueService<T>
    {
        private List<Action<T>> subscriptions = new List<Action<T>>();

        public void Publish(T value)
        {
            foreach (var sub in subscriptions)
            {
                sub(value);
            }
        }

        public void Subscribe(Action<T> subscription)
        {
            subscriptions.Add(subscription);
        }
    }
}