using System.Threading.Tasks;

namespace AutoScheduler.Common.Handlers
{
    public interface IHandler<In, Out>
    {
        Task<Out> Handle(In input);
    }
}