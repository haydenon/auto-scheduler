using System;
using System.Threading.Tasks;

namespace AutoScheduler.Common.Handlers
{
    public class HandlerBridge<In, Out>
    {
        private readonly Func<In, Task<Out?>> handle;

        private HandlerBridge(
            Func<In, Task<Out?>> handle)
        {
            this.handle = handle;
        }

        public Task<Out?> Handle(In input)
            => handle(input);

        public static HandlerBridge<In, Out> CreateBridge<TIn>(IHandler<TIn, Out> handler)
            where TIn : In
        {
            return new HandlerBridge<In, Out>(
                async (input) =>
                {
                    if (input is TIn handlerInput)
                    {
                        return await handler.Handle(handlerInput);
                    }

                    return default(Out);
                }
            );
        }
    }
}