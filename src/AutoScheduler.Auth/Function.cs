﻿using Google.Cloud.Functions.Framework;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AutoScheduler.Auth
{
    public class Function : IHttpFunction
    {
        private static JsonSerializerOptions serializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };

        private static JsonSerializerOptions tokenSerializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = SnakeCaseNamingPolicy.Instance
        };

        public async Task HandleAsync(HttpContext context)
        {
            AuthRequest request;
            try
            {
                using var reader = new StreamReader(context.Request.Body, Encoding.UTF8);
                var content = await reader.ReadToEndAsync();
                request = JsonSerializer.Deserialize<AuthRequest>(content, serializerOptions);
            }
            catch (Exception)
            {
                await ReturnError(context, "Invalid request body", HttpStatusCode.BadRequest);
                return;
            }

            if (string.IsNullOrWhiteSpace(request.Code) || request.Code.Length < 40)
            {
                await ReturnError(context, "Invalid code", HttpStatusCode.BadRequest);
                return;
            }

            var configuration = GetConfiguration();

            var (success, response) = await SendTokenReqeuest(configuration, request.Code);
            if (!success)
            {
                await ReturnError(context, "Auth request failed", HttpStatusCode.BadRequest);
                return;
            }

            var authResp = new AuthResponse
            {
                IdToken = response.IdToken
            };

            await WriteToResponse(context, authResp);
        }

        private async Task<(bool, TokenResponse)> SendTokenReqeuest(IConfiguration configuration, string code)
        {
            var oauthSection = configuration.GetSection("OAuth");
            var client = new HttpClient();
            var content = new FormUrlEncodedContent(
                new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("client_id", oauthSection.GetValue<string>("ClientId")),
                    new KeyValuePair<string, string>("client_secret", oauthSection.GetValue<string>("ClientSecret")),
                    new KeyValuePair<string, string>("code", code),
                    new KeyValuePair<string, string>("grant_type", "authorization_code"),
                    new KeyValuePair<string, string>("redirect_uri", "postmessage")
                }
            );

            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            content.Headers.ContentType.CharSet = "UTF-8";
            client.DefaultRequestHeaders.ExpectContinue = false;
            var response = await client.PostAsync(new Uri(oauthSection.GetValue<string>("Endpoint")), content);
            if (!response.IsSuccessStatusCode)
            {
                return (false, null);
            }

            var respContent = await response.Content.ReadAsStringAsync();
            try
            {
                var tokenResponse = JsonSerializer.Deserialize<TokenResponse>(respContent, tokenSerializerOptions);
                return (true, tokenResponse);
            }
            catch
            {
                return (false, null);
            }
        }

        private IConfiguration GetConfiguration()
            => new ConfigurationBuilder().AddEnvironmentVariables().Build();

        private async Task ReturnError(HttpContext context, string message, HttpStatusCode statusCode)
        {
            var problem = new ProblemDetails
            {
                Status = (int)statusCode,
                Detail = message
            };

            var apiBehaviourOptions = new ApiBehaviorOptions();
            if (ProblemDetailValues.ContainsKey((int)statusCode))
            {
                var (link, title) = ProblemDetailValues[(int)statusCode];
                problem.Title = title;
                problem.Type = link;
            }
            await WriteToResponse(context, problem, statusCode);
        }

        private async Task WriteToResponse<T>(HttpContext context, T response, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            var result = JsonSerializer.Serialize(response, serializerOptions);
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.StatusCode = (int)statusCode;
            await context.Response.WriteAsync(result);
        }

        public class AuthRequest
        {
            public string Code { get; set; }
        }

        public class AuthResponse
        {
            public string IdToken { get; set; }
        }

        public static readonly Dictionary<int, (string Type, string Title)> ProblemDetailValues = new Dictionary<int, (string Type, string Title)>
        {
            [400] =
                (
                    "https://tools.ietf.org/html/rfc7231#section-6.5.1",
                    "Bad Request"
                ),
            [401] =
                (
                    "https://tools.ietf.org/html/rfc7235#section-3.1",
                    "Unauthorized"
                ),
            [403] =
                (
                    "https://tools.ietf.org/html/rfc7231#section-6.5.3",
                    "Forbidden"
                ),
            [404] =
                (
                    "https://tools.ietf.org/html/rfc7231#section-6.5.4",
                    "Not Found"
                ),
            [500] =
                (
                    "https://tools.ietf.org/html/rfc7231#section-6.6.1",
                    "An error occurred while processing your request."
                ),
        };
    }

    public class TokenResponse
    {
        public string AccessToken { get; set; }
        public long ExpiresIn { get; set; }
        public string RefreshToken { get; set; }
        public string Scope { get; set; }
        public string TokenType { get; set; }
        public string IdToken { get; set; }
    }

    public static class StringUtils
    {
        public static string ToSnakeCase(this string str)
        {
            return string.Concat(str.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString())).ToLower();
        }
    }


    public class SnakeCaseNamingPolicy : JsonNamingPolicy
    {
        public static SnakeCaseNamingPolicy Instance { get; } = new SnakeCaseNamingPolicy();

        public override string ConvertName(string name)
            => name.ToSnakeCase();
    }
}