using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Notion.Common;
using LanguageExt;
using static LanguageExt.Prelude;

namespace AutoScheduler.Core
{
    public static class MappingExtensions
    {
        public static Either<IError, T> GetEnumValueFromDisplay<T>(this string? name)
        {
            if (name == null)
            {
                return new ValidationError("Value is null");
            }

            var type = typeof(T);
            if (!type.IsEnum)
            {
                throw new InvalidOperationException();
            }

            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DisplayAttribute)) as DisplayAttribute;
                if (attribute != null)
                {
                    if (attribute.Name == name)
                    {
                        return (T)field.GetValue(null)!;
                    }
                }
                else
                {
                    if (field.Name == name)
                    {
                        return (T)field.GetValue(null)!;
                    }
                }
            }

            return new ValidationError($"'{name}' is not a valid value for '{type.Name}'");
        }

        public static Either<IError, T> GetValue<T>(this Nullable<T> value)
            where T : struct
        {
            if (!value.HasValue)
            {
                return new ValidationError("Value is null");
            }

            return value.Value;
        }

        public static Either<IError, T> GetValue<T>(this T? value)
        {
            if (value == null)
            {
                return new ValidationError("Value is null");
            }

            return value;
        }

        public static Either<IError, DateTime> GetDateTime(this string? value)
        {
            if (value == null)
            {
                return new ValidationError("Value is null");
            }

            return DateTimeOffset.TryParseExact(
                value,
                new string[] { "yyyy-MM-dd'T'HH:mm:ss.FFFK" },
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out var result
            ) ? result.UtcDateTime : new ValidationError("Invalid date time value");
        }

        public static Either<IError, Guid> GetGuid(this string? value)
        {
            if (!Guid.TryParse(value, out var guid))
            {
                return new ValidationError("Value is null");
            }

            return guid;
        }

        public static Either<IError, Uri> GetUri(this string? value)
        {
            if (value == null)
            {
                return new ValidationError("Value is null");
            }

            return new Uri(value, UriKind.Absolute);
        }

        public static Either<IError, string> MapTitleToString(this List<TextObject> values)
            => string.Join("", values.Select(t => t.PlainText ?? string.Empty) ?? new string[] { });

        public static string? MapIcon(this IconData? data)
        {
            if (data?.Emoji == null)
            {
                return default(string?);
            }

            return data.Emoji;
        }

        public static Either<IError, ICollection<T>> AsCollection<T>(this Either<IError, IList<T>> value)
            => value.Map(v => (ICollection<T>)v);

        public static Either<IError, ICollection<R>> MapToCollection<T, R>(this Either<IError, IList<T>> values, Func<T, Either<IError, R>> map)
        {
            return values.Bind(v => v.Aggregate(new List<R>(), (Either<IError, ICollection<R>> res, T next) =>
            {
                return res.Bind(
                    (results) =>
                        map(next).Map(
                            (mapped) =>
                            {
                                results.Add(mapped);
                                return results;
                            }
                        )
                );
            }));
        }

        public static Either<IError, IList<T>> GetAndMapValues<S, T>(this IList<S>? values, Func<S, Either<IError, T>> map)
        {
            if (values == null)
            {
                return new ValidationError("No values provided");
            }

            return values.Aggregate(new List<T>(), (Either<IError, IList<T>> res, S next) =>
            {
                return res.Bind(
                    (results) =>
                        map(next).Map(
                            (mapped) =>
                            {
                                results.Add(mapped);
                                return results;
                            }
                        )
                );
            });
        }
    }
}