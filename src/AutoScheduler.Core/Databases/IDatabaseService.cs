using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Core.Databases
{
    public interface IDatabaseService
    {
        Task<Either<IError, ICollection<Database>>> GetAllDatabasesForCurrentUser();

        Task<Either<IError, ICollection<Database>>> GetAllDatabasesForUser(string userId);
    }
}