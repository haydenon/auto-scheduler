using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data;
using AutoScheduler.Data.Notion.Api;
using AutoScheduler.Data.Notion.Api.Client;
using AutoScheduler.Data.Notion.Databases;
using LanguageExt;

namespace AutoScheduler.Core.Databases
{
    public class DatabaseService : IDatabaseService
    {
        private readonly IRepositoryFactory<IListRepository<DatabaseData>> databaseRepositoryFactory;
        private readonly INotionApiClientFactory notionApiClientFactory;

        public DatabaseService(
            INotionApiClientFactory notionApiClientFactory,
            IRepositoryFactory<IListRepository<DatabaseData>> databaseRepositoryFactory)
        {
            this.notionApiClientFactory = notionApiClientFactory;
            this.databaseRepositoryFactory = databaseRepositoryFactory;
        }

        private async Task<IListRepository<DatabaseData>> DatabaseRepository()
        {
            var client = await notionApiClientFactory.GetClientForCurrentUser();
            return databaseRepositoryFactory.Create(client, null!);
        }

        private async Task<IListRepository<DatabaseData>> DatabaseRepository(string userId)
        {
            var client = await notionApiClientFactory.GetClientForUser(userId);
            return databaseRepositoryFactory.Create(client, null!);
        }

        public Task<Either<IError, ICollection<Database>>> GetAllDatabasesForCurrentUser()
            => GetAllDatabases(DatabaseRepository);

        public Task<Either<IError, ICollection<Database>>> GetAllDatabasesForUser(string userId)
            => GetAllDatabases(() => DatabaseRepository(userId));

        private async Task<Either<IError, ICollection<Database>>> GetAllDatabases(Func<Task<IListRepository<DatabaseData>>> getRepo)
        {
            var repo = await getRepo();
            var allDatabases = await repo.FetchAll();
            return allDatabases.Bind(databases => databases.GetAndMapValues(db => db.MapToModel()).AsCollection());
        }
    }
}