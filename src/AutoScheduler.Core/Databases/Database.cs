using System;
using System.Collections.Generic;
using AutoScheduler.Core.Databases.Properties;

namespace AutoScheduler.Core.Databases
{
    public record Database(
        Guid Id,
        string Title,
        ICollection<IPropertyDefinition> Properties,
        Uri Url,
        string? Icon
    );
}