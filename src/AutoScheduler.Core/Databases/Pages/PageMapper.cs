using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Notion.Databases.Pages;
using LanguageExt;

namespace AutoScheduler.Core.Databases.Pages
{
    public static class PageMapperExtensions
    {
        public static Either<IError, PageDetails> MapToPageDetails(this PageData data)
            => from id in data.Id.GetValue()
               from url in data.Url.GetUri()
               select new PageDetails(id, url, data.Icon.MapIcon());
    }
}