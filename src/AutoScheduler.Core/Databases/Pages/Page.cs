using System;

namespace AutoScheduler.Core.Databases.Pages
{
    public record PageDetails(
        Guid Id,
        Uri Url,
        string? Icon
    );

    public interface IPage
    {
        PageDetails PageDetails { get; }
    }
}