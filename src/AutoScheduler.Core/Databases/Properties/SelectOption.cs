namespace AutoScheduler.Core.Databases.Properties
{
    public record SelectOption(
        string Id,
        string Name,
        OptionColor Color
    );
}