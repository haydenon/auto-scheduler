using System.ComponentModel.DataAnnotations;

namespace AutoScheduler.Core.Databases.Properties
{

    public interface IPropertyBase
    {
        public string Id { get; }
        public PropertyType PropertyType { get; }
    }

    public enum PropertyType
    {
        [Display(Name = "title")]
        Title,
        [Display(Name = "rich_text")]
        RichText,
        [Display(Name = "number")]
        Number,
        [Display(Name = "select")]
        Select,
        [Display(Name = "multi_select")]
        MultiSelect,
        [Display(Name = "date")]
        Date,
        [Display(Name = "people")]
        People,
        [Display(Name = "files")]
        Files,
        [Display(Name = "checkbox")]
        Checkbox,
        [Display(Name = "url")]
        Url,
        [Display(Name = "email")]
        Email,
        [Display(Name = "phone_number")]
        PhoneNumber,
        [Display(Name = "formula")]
        Formula,
        [Display(Name = "relation")]
        Relation,
        [Display(Name = "rollup")]
        Rollup,
        [Display(Name = "created_time")]
        CreatedTime,
        [Display(Name = "created_by")]
        CreatedBy,
        [Display(Name = "last_edited_time")]
        LastEditedTime,
        [Display(Name = "last_edited_by")]
        LastEditedBy
    }

    public enum OptionColor
    {
        [Display(Name = "default")]
        DefaultColor,
        [Display(Name = "gray")]
        Gray,
        [Display(Name = "brown")]
        Brown,
        [Display(Name = "orange")]
        Orange,
        [Display(Name = "yellow")]
        Yellow,
        [Display(Name = "green")]
        Green,
        [Display(Name = "blue")]
        Blue,
        [Display(Name = "purple")]
        Purple,
        [Display(Name = "pink")]
        Pink,
        [Display(Name = "red")]
        Red
    }
}