using System;
using System.Collections.Generic;
using AutoScheduler.Common.Models;

namespace AutoScheduler.Core.Databases.Properties
{
    public interface IPropertyValue : IPropertyBase
    {
    }

    public record PropertyValue(
        string Id,
        PropertyType PropertyType
    ) : IPropertyValue;

    public record SelectPropertyValue(
        string Id,
        PropertyType PropertyType,
        ICollection<SelectOption> Options
    ) : IPropertyValue;

    public record DateValue(
        IDateTimeValue Start,
        IDateTimeValue? End
    );

    public record DatePropertyValue(
        string Id,
        PropertyType PropertyType,
        DateValue Date
    ) : IPropertyValue;

    public record TitlePropertyValue(
        string Id,
        PropertyType PropertyType,
        string Title
    ) : IPropertyValue;
}