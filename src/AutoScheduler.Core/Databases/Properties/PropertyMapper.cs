using System.Collections.Generic;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Notion.Databases.Properties;
using LanguageExt;

namespace AutoScheduler.Core.Databases.Properties
{
    public static class PropertyMapper
    {
        private static Either<IError, IPropertyDefinition> MapStandardProperty(this PropertyDefinitionData data)
            => from id in data.Id.GetValue()
               from propType in data.Type.GetEnumValueFromDisplay<PropertyType>()
               from name in data.Name.GetValue()
               select new PropertyDefinition(id, propType, name) as IPropertyDefinition;
        private static Either<IError, SelectOption> MapSelectOption(this SelectOptionData data)
            => from id in data.Id.GetValue()
               from name in data.Name.GetValue()
               from color in data.Color.GetEnumValueFromDisplay<OptionColor>()
               select new SelectOption(id, name, color);

        private static Either<IError, IPropertyDefinition> MapSelectProperty(this PropertyDefinitionData data)
            => from id in data.Id.GetValue()
               from propType in data.Type.GetEnumValueFromDisplay<PropertyType>()
               from name in data.Name.GetValue()
               from options in ((data.Select ?? data.MultiSelect)?.Options).GetAndMapValues(opts => opts.MapSelectOption())
               select new SelectPropertyDefinition(id, propType, name, options) as IPropertyDefinition;


        public static Either<IError, IPropertyDefinition> MapProperty(this PropertyDefinitionData data)
            => data.Type switch
            {
                "select" or "multi_select" => data.MapSelectProperty(),
                _ => data.MapStandardProperty()
            };

        private static Either<IError, IPropertyValue> MapStandardPropertyValue(this PropertyValueData data)
            => from id in data.Id.GetValue()
               from propType in data.Type.GetEnumValueFromDisplay<PropertyType>()
               select new PropertyValue(id, propType) as IPropertyValue;

        private static Either<IError, IPropertyValue> MapSelectPropertyValue(this PropertyValueData data)
            => from id in data.Id.GetValue()
               from propType in data.Type.GetEnumValueFromDisplay<PropertyType>()
               from options in ((data.Select != null ? new List<SelectOptionData> { data.Select } : data.MultiSelect)).GetAndMapValues(opts => opts.MapSelectOption())
               select new SelectPropertyValue(id, propType, options) as IPropertyValue;

        private static Either<IError, DateValue> MapDateValue(DateValueData data)
            => from start in data.Start.GetValue()
               select new DateValue(start, data.End);

        private static Either<IError, IPropertyValue> MapDatePropertyValue(this PropertyValueData data)
            => from id in data.Id.GetValue()
               from propType in data.Type.GetEnumValueFromDisplay<PropertyType>()
               from date in data.Date.GetValue().Bind(MapDateValue)
               select new DatePropertyValue(id, propType, date) as IPropertyValue;

        public static Either<IError, IPropertyValue> MapTitlePropertyValue(this PropertyValueData data)
            => from id in data.Id.GetValue()
               from propType in data.Type.GetEnumValueFromDisplay<PropertyType>()
               from title in data.Title.GetValue().Bind(MappingExtensions.MapTitleToString)
               select new TitlePropertyValue(id, propType, title) as IPropertyValue;

        public static Either<IError, IPropertyValue> MapPropertyValue(this PropertyValueData data)
            => data.Type switch
            {
                "select" or "multi_select" => data.MapSelectPropertyValue(),
                "date" => data.MapDatePropertyValue(),
                "title" => data.MapTitlePropertyValue(),
                _ => data.MapStandardPropertyValue()
            };
    }
}