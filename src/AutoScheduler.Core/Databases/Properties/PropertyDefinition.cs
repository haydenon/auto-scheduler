using System.Collections.Generic;

namespace AutoScheduler.Core.Databases.Properties
{
    public interface IPropertyDefinition : IPropertyBase
    {
        public string Name { get; }
    }

    public record PropertyDefinition(
        string Id,
        PropertyType PropertyType,
        string Name
    ) : IPropertyDefinition;

    public record SelectPropertyDefinition(
        string Id,
        PropertyType PropertyType,
        string Name,
        ICollection<SelectOption> Options
    ) : IPropertyDefinition;
}