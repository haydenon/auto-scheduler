using System.Collections.Generic;
using System.Linq;
using AutoScheduler.Common.Errors;
using AutoScheduler.Core.Databases.Properties;
using AutoScheduler.Data.Notion.Databases;
using AutoScheduler.Data.Notion.Databases.Properties;
using LanguageExt;

namespace AutoScheduler.Core.Databases
{
    public static class DatabaseMapperExtensions
    {
        private static List<PropertyDefinitionData> PropertyList(this DatabaseData data)
            => data.Properties?.Select(p => p.Value).ToList() ?? new List<PropertyDefinitionData>();

        public static Either<IError, Database> MapToModel(this DatabaseData data)
            => from id in data.Id.GetValue()
               from title in data.Title.GetValue().Bind(title => title.MapTitleToString())
               from properties in data.PropertyList().GetAndMapValues(prop => prop.MapProperty())
               from url in data.Url.GetUri()
               select new Database(id, title, properties, url, data.Icon.MapIcon());
    }
}