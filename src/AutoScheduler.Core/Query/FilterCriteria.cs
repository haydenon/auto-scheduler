using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AutoScheduler.Core.Query
{
    public interface IFilter
    {
    }

    public interface IPropertyFilter : IFilter
    {
        string Property { get; }
    }

    public record AndFilter(IList<IFilter> And) : IFilter;
    public record OrFilter(IList<IFilter> Or) : IFilter;

    public interface ISelectCriteria { }
    public record SelectFilter(string Property, ISelectCriteria Select) : IPropertyFilter;
    public record SelectEqualsCriteria(string EqualsValue) : ISelectCriteria;
}