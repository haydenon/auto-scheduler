using System;
using System.Collections.Generic;
using System.Linq;
using AutoScheduler.Data.Notion.Databases.Pages;

namespace AutoScheduler.Core.Query
{
    public static class CriteriaMapperExtensions
    {
        public static IFilterData MapToData(this IFilter filter)
            => filter switch
            {
                OrFilter { Or: IList<IFilter> filters } => new OrFilterData { Or = filters.Select(MapToData).ToList() },
                AndFilter { And: IList<IFilter> filters } => new AndFilterData { And = filters.Select(MapToData).ToList() },
                SelectFilter select => select.MapToData(),
                _ => throw new NotImplementedException()
            };

        public static PropertyFilterData MapToData(this SelectFilter select)
            => new PropertyFilterData
            {
                Property = select.Property,
                Critera = select.Select.MapToData()
            };

        public static Dictionary<string, object> MapToData(this ISelectCriteria criteria)
        {
            var key = criteria switch
            {
                _ => "equals"
            };
            var value = criteria switch
            {
                SelectEqualsCriteria { EqualsValue: string eqValue } => eqValue,
                _ => ""
            };

            var criteriaData = new CriteriaData
            {
                Values = new Dictionary<string, object> { { key, value } }
            };
            return new Dictionary<string, object> {
                { "select",criteriaData }
            };
        }
    }
}