using System;
using System.Collections.Generic;

namespace AutoScheduler.Core.DatabaseConfigurations
{
    public record StatusProperty(
        string PropertyId,
        string DefaultStatusId,
        ICollection<string> InProgressStatusIds,
        ICollection<string> FinishedStatusIds
    );

    public record NotionTaskList(
        Guid DatabaseId,
        string UserId,
        string ScheduledDatePropertyId,
        StatusProperty StatusProperty
    );
}