using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.DatabaseConfiguration;
using LanguageExt;

namespace AutoScheduler.Core.DatabaseConfigurations
{
    public class DatabaseConfigurationService : IDatabaseConfigurationService
    {
        private readonly IDatabaseConfigurationRepository databaseConfigurationRepository;

        public DatabaseConfigurationService(IDatabaseConfigurationRepository databaseConfigurationRepository)
        {
            this.databaseConfigurationRepository = databaseConfigurationRepository;
        }

        public async Task<Either<IError, DatabaseConfiguration>> GetDatabaseConfigurationForUser(string userId)
        {
            var data = await databaseConfigurationRepository.GetDatabaseConfigurationForUser(userId);
            return data.GetValue().Bind(dc => dc.MapToModel());
        }

        public IAsyncEnumerable<DatabaseConfiguration> GetAllDatabaseConfigurations()
            => databaseConfigurationRepository.GetAllDatabaseConfigurations()
                .SelectMany(dc => dc.MapToModel().RightAsEnumerable().ToAsyncEnumerable());
    }
}