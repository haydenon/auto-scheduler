using AutoScheduler.Common.Errors;
using AutoScheduler.Data.DatabaseConfiguration;
using LanguageExt;

namespace AutoScheduler.Core.DatabaseConfigurations
{
    public static class DatabaseConfigurationMapperExtensions
    {
        public static Either<IError, DatabaseConfiguration> MapToModel(this DatabaseConfigurationData data)
            => from userId in data.UserId.GetValue()
               from databaseList in data.TaskListDatabases.GetAndMapValues(tld => tld.MapToModel(userId))
               select new DatabaseConfiguration(userId, databaseList);
    }
}