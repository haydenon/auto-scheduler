using System.Linq;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.DatabaseConfiguration.TaskList;
using LanguageExt;

namespace AutoScheduler.Core.DatabaseConfigurations
{
    public static class TaskListMapperExtensions
    {
        public static Either<IError, StatusProperty> MapToModel(this StatusPropertyData data)
            => from finishedStatusIds in data.FinishedStatusIds.GetValue()
               from inProgressStatusIds in data.InProgressStatusIds.GetValue()
               from defaultStatusId in data.DefaultStatusId.GetValue()
               from propertyId in data.PropertyId.GetValue()
               select new StatusProperty(propertyId, defaultStatusId, inProgressStatusIds, finishedStatusIds);

        public static Either<IError, NotionTaskList> MapToModel(this TaskListDatabaseData data, string userId)
            => from databaseId in data.DatabaseId.GetGuid()
               from scheduledDatePropertyId in data.ScheduledDatePropertyId.GetValue()
               from statusProperty in data.StatusProperty.GetValue().Bind(sp => sp.MapToModel())
               select new NotionTaskList(databaseId, userId, scheduledDatePropertyId, statusProperty);

        public static StatusPropertyData MapToData(this StatusProperty model)
            => new StatusPropertyData
            {
                PropertyId = model.ToString(),
                DefaultStatusId = model.DefaultStatusId,
                InProgressStatusIds = model.InProgressStatusIds.ToList(),
                FinishedStatusIds = model.FinishedStatusIds.ToList()
            };

        public static TaskListDatabaseData MapToData(this NotionTaskList model)
            => new TaskListDatabaseData
            {
                DatabaseId = model.DatabaseId.ToString(),
                ScheduledDatePropertyId = model.ScheduledDatePropertyId,
                StatusProperty = model.StatusProperty.MapToData()
            };
    }
}