using System.Collections.Generic;

namespace AutoScheduler.Core.DatabaseConfigurations
{
    public record DatabaseConfiguration(
        string UserId,
        ICollection<NotionTaskList> TaskListDatabases
    );
}