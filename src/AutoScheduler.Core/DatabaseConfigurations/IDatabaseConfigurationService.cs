using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Core.DatabaseConfigurations
{
    public interface IDatabaseConfigurationService
    {
        Task<Either<IError, DatabaseConfiguration>> GetDatabaseConfigurationForUser(string userId);

        IAsyncEnumerable<DatabaseConfiguration> GetAllDatabaseConfigurations();
    }
}