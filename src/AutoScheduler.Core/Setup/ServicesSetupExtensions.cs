using AutoScheduler.Core.DatabaseConfigurations;
using AutoScheduler.Core.Databases;
using AutoScheduler.Core.Tasks;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServicesSetupExtensions
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<ITaskService, TaskService>();

            services.AddScoped<IDatabaseConfigurationService, DatabaseConfigurationService>();

            services.AddScoped<IDatabaseService, DatabaseService>();
        }
    }
}