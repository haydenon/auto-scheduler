using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Tasks;
using LanguageExt;

namespace AutoScheduler.Core.Tasks;

public static class TaskMapper
{
    public static TaskDataFilter MapToData(this TaskFilter model)
        => new TaskDataFilter(
            model.MinTime,
            model.MaxTime
        );

    public static Either<IError, TaskTime> MapToTimeModel(this TaskData data)
        => from start in data.Start.GetValue()
           from end in data.End.GetValue()
           select new TaskTime(start, end, data.IsRecurring);

    public static Either<IError, TaskItem> MapToModel(this TaskData data)
        => from id in data.TaskId.GetValue()
           from source in data.Source.GetEnumValueFromDisplay<TaskSource>()
           from title in data.Title.GetValue()
           from time in data.MapToTimeModel()
           select new TaskItem(id, source, title, time);

}