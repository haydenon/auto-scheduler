using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Core.Tasks;
public interface ITaskService
{
    Task<Either<IError, ICollection<TaskItem>>> GetTasksForUser(string userId, TaskFilter filter);
}