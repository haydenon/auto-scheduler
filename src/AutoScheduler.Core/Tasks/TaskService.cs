using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data;
using AutoScheduler.Data.Google;
using AutoScheduler.Data.Notion.Api;
using AutoScheduler.Data.Notion.Api.Client;
using AutoScheduler.Data.Tasks;
using LanguageExt;

namespace AutoScheduler.Core.Tasks;

public record TaskFilter(
    DateTime? MinTime,
    DateTime? MaxTime
);

public class TaskService : ITaskService
{
    private readonly IRepositoryFactory<ITaskSourceRepository> taskSourceRepositoryFactory;
    private readonly INotionApiClientFactory notionApiClientFactory;
    private readonly IGoogleApiClientFactory googleApiClientFactory;

    public TaskService(
        IRepositoryFactory<ITaskSourceRepository> taskSourceRepositoryFactory,
        INotionApiClientFactory notionApiClientFactory,
        IGoogleApiClientFactory googleApiClientFactory)
    {
        this.taskSourceRepositoryFactory = taskSourceRepositoryFactory;
        this.notionApiClientFactory = notionApiClientFactory;
        this.googleApiClientFactory = googleApiClientFactory;
    }

    public async Task<Either<IError, ICollection<TaskItem>>> GetTasksForUser(string userId, TaskFilter filter)
    {
        var client = await notionApiClientFactory.GetClientForUser(userId);
        var googleClient = googleApiClientFactory.GetClientForUser(userId);
        var taskSourceRepository = taskSourceRepositoryFactory.Create(client, googleClient);
        var data = await taskSourceRepository.GetTasksForUser(userId, filter.MapToData());
        return data.Bind(tasks => tasks.GetAndMapValues(t => t.MapToModel()).AsCollection());
    }
}