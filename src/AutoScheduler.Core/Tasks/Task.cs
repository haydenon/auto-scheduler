using System;
using System.ComponentModel.DataAnnotations;
using AutoScheduler.Common.Models;

namespace AutoScheduler.Core.Tasks;

public record TaskTime(
    IDateTimeValue Start,
    IDateTimeValue End,
    bool IsRecurring
);

public enum TaskSource
{
    [Display(Name = "google_calendar")]
    GoogleCalendar,
    [Display(Name = "google_tasks")]
    GoogleTasks,
    [Display(Name = "notion")]
    Notion
}

public record TaskItem(
    string TaskId,
    TaskSource TaskSource,
    string Title,
    TaskTime Time
);