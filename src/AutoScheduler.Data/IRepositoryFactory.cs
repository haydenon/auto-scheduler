using AutoScheduler.Data.Google;
using AutoScheduler.Data.Notion.Api.Client;

namespace AutoScheduler.Data
{
    public interface IRepositoryFactory<T>
    {
        T Create(INotionClient notionClient, IGoogleClient googleClient);
    }
}