using System.Collections.Generic;
using System.Threading.Tasks;
using LanguageExt;

namespace AutoScheduler.Data.Storage
{
    public interface IStoreCollection<T>
    {
        Task<T?> GetAsync(string documentId);

        Task<Unit> SetAsync(string documentId, T item);

        Task<Unit> DeleteAsync(string documentId);

        IAsyncEnumerable<T> GetAllAsync();

        IAsyncEnumerable<T> WhereEqualTo(string property, object value);
    }
}