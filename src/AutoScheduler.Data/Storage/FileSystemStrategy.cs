using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using LanguageExt;
using static LanguageExt.Prelude;

namespace AutoScheduler.Data.Storage
{
    public class FileSystemStrategy : IStoreStrategy
    {
        private readonly string directory;

        public FileSystemStrategy(string directory)
        {
            this.directory = directory;
        }

        public IStoreCollection<T> Collection<T>(string collectionName)
        {
            var collectionDirectory = $"{directory}/database/{collectionName}";
            return new FileSystemCollection<T>(collectionDirectory);
        }
    }

    public class FileSystemCollection<T> : IStoreCollection<T>
    {
        private readonly string directory;

        public FileSystemCollection(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            this.directory = directory;
        }

        private string FilePath(string fileName) => $"{directory}/{fileName}.json";

        public Task<Unit> DeleteAsync(string documentId)
        {
            File.Delete(FilePath(documentId));
            return Task.FromResult(unit);
        }

        public async IAsyncEnumerable<T> GetAllAsync()
        {
            var files = Directory.GetFiles(directory);
            foreach (var file in files)
            {
                var contents = await File.ReadAllTextAsync(file);
                yield return JsonSerializer.Deserialize<T>(contents)!;
            }
        }

        public async Task<T?> GetAsync(string documentId)
        {
            var path = FilePath(documentId);
            if (!File.Exists(path))
            {
                return default;
            }

            var contents = await File.ReadAllTextAsync(path);
            return JsonSerializer.Deserialize<T>(contents);
        }

        public async Task<Unit> SetAsync(string documentId, T item)
        {
            var contents = JsonSerializer.Serialize(item);
            await File.WriteAllTextAsync(FilePath(documentId), contents);
            return unit;
        }

        public async IAsyncEnumerable<T> WhereEqualTo(string property, object value)
        {
            var propMatches = PropertyMatches(property, value);
            await foreach (var item in GetAllAsync())
            {
                if (propMatches(item))
                {
                    yield return item;
                }
            }
        }

        private Func<T, bool> PropertyMatches(string property, object value)
        {
            var prop = typeof(T).GetProperty(property);
            return (T item) =>
            {
                var itemValue = prop!.GetValue(item);
                return value.Equals(itemValue);
            };
        }
    }
}