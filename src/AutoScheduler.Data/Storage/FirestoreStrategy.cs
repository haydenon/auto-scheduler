using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Cloud.Firestore;
using LanguageExt;
using static LanguageExt.Prelude;

namespace AutoScheduler.Data.Storage
{
    public class FirestoreStrategy : IStoreStrategy
    {
        private readonly FirestoreDb client;

        public FirestoreStrategy(FirestoreDb client)
        {
            this.client = client;
        }

        public IStoreCollection<T> Collection<T>(string collectionName)
            => new FirestoreCollection<T>(client, collectionName);
    }

    public class FirestoreCollection<T> : IStoreCollection<T>
    {
        private readonly FirestoreDb client;
        private readonly string collectionName;

        public FirestoreCollection(FirestoreDb client, string collectionName)
        {
            this.client = client;
            this.collectionName = collectionName;
        }

        public async Task<Unit> DeleteAsync(string documentId)
        {
            await client.Collection(collectionName).Document(documentId).DeleteAsync();
            return unit;
        }

        public async IAsyncEnumerable<T> GetAllAsync()
        {

            await foreach (var doc in client.Collection(collectionName).ListDocumentsAsync())
            {
                var snapshot = await doc.GetSnapshotAsync();
                yield return snapshot.ConvertTo<T>();
            }
        }

        public async Task<T?> GetAsync(string documentId)
        {
            var snapshot = await client.Collection(collectionName).Document(documentId).GetSnapshotAsync();
            return snapshot.ConvertTo<T>();
        }

        public async Task<Unit> SetAsync(string documentId, T item)
        {
            await client.Collection(collectionName).Document(documentId).SetAsync(item);
            return unit;
        }

        public async IAsyncEnumerable<T> WhereEqualTo(string property, object value)
        {
            var query = client.Collection(collectionName).WhereEqualTo(property, value);
            await foreach (var snapshot in query.StreamAsync())
            {
                yield return snapshot.ConvertTo<T>();
            }
        }
    }
}