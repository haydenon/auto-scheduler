namespace AutoScheduler.Data.Storage
{
    public interface IStoreStrategy
    {
        IStoreCollection<T> Collection<T>(string collectionName);
    }
}