using System.Collections.Generic;
using AutoScheduler.Common.Models;

namespace AutoScheduler.Data.Google.Events
{
    public class TimeData
    {
        public IDateTimeValue? DateTime { get; init; }
        public IDateTimeValue? Date { get; init; }
    }

    public class EventData
    {
        public string? Id { get; init; }
        public string? Summary { get; init; }
        public TimeData? Start { get; init; }
        public TimeData? End { get; init; }
        public List<string>? Recurrence { get; init; }
    }
}