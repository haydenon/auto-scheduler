using System;

namespace AutoScheduler.Data.Google.Tasks;

public class TaskData
{
    public string? Id { get; init; }
    public string? Title { get; init; }
    public DateTime? Due { get; set; }
    public string? Status { get; set; }
}