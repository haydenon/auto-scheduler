using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Cache;
using LanguageExt;

namespace AutoScheduler.Data.Google;

public class TokenCache : DataCache<GoogleAccessToken>
{
    public override async Task<GoogleAccessToken?> GetValue(string id, Func<Task<GoogleAccessToken?>> getter)
    {
        var value = await base.GetValue(id, getter);
        if (value != null && DateTime.UtcNow >= value.ExpiresAt)
        {
            var newValue = await getter();
            if (newValue != null)
            {
                base.UpdateValue(id, newValue);
            }
            return newValue;
        }

        return value;
    }

    public override async Task<Either<IError, GoogleAccessToken>> GetValue(string id, Func<Task<Either<IError, GoogleAccessToken>>> getter)
    {
        var value = await base.GetValue(id, getter);
        return await value.BindAsync(async token =>
        {
            if (DateTime.UtcNow >= token.ExpiresAt)
            {
                var newValue = await getter();
                newValue.Do(val => base.UpdateValue(id, val));
                return newValue;
            }

            return token;
        });
    }
}