using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using AutoScheduler.Common;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Cache;
using AutoScheduler.Data.GoogleOAuth;
using LanguageExt;
using Microsoft.Extensions.Configuration;

namespace AutoScheduler.Data.Google;
public class GoogleTokenService : IGoogleTokenService
{
    private static readonly HttpClient client = new HttpClient();
    private readonly IDataCache<GoogleAccessToken> cache;
    private readonly IGoogleOAuthRepository googleOAuthRepository;
    private readonly IConfiguration configuration;

    private static JsonSerializerOptions tokenSerializerOptions = new JsonSerializerOptions
    {
        PropertyNameCaseInsensitive = true,
        PropertyNamingPolicy = SnakeCaseNamingPolicy.Instance
    };

    public GoogleTokenService(
        IDataCache<GoogleAccessToken> cache,
        IGoogleOAuthRepository googleOAuthRepository,
        IConfiguration configuration)
    {
        this.cache = cache;
        this.googleOAuthRepository = googleOAuthRepository;
        this.configuration = configuration;
    }

    public Task<Either<IError, GoogleAccessToken>> GetAccessToken(string userId)
        => cache.GetValue(userId, GetAccessTokenFromRefreshToken(userId));

    private Func<Task<Either<IError, GoogleAccessToken>>> GetAccessTokenFromRefreshToken(string userId)
        => async () =>
        {
            var oauth = await googleOAuthRepository.GetGoogleOAuthForUser(userId);
            if (oauth?.RefreshToken == null)
            {
                return new GoogleApisNotConfiguredError();
            }

            var oauthSection = configuration.GetSection("OAuth");
            var content = new FormUrlEncodedContent(
                new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("client_id", oauthSection.GetValue<string>("ClientId")),
                    new KeyValuePair<string, string>("client_secret", oauthSection.GetValue<string>("ClientSecret")),
                    new KeyValuePair<string, string>("refresh_token", oauth.RefreshToken),
                    new KeyValuePair<string, string>("grant_type", "refresh_token"),
                }
            );

            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            content.Headers.ContentType.CharSet = "UTF-8";
            client.DefaultRequestHeaders.ExpectContinue = false;
            var response = await client.PostAsync(new Uri(oauthSection.GetValue<string>("Endpoint")), content);
            var respContent = await response.Content.ReadAsStringAsync();
            try
            {
                var tokenResponse = JsonSerializer.Deserialize<GoogleAccessTokenResponse>(respContent, tokenSerializerOptions);
                var refreshToken = tokenResponse?.AccessToken;
                if (refreshToken != null)
                {
                    var expiresAt = DateTime.UtcNow.AddSeconds(tokenResponse?.ExpiresIn ?? 60);
                    return new GoogleAccessToken(refreshToken, expiresAt);
                }

                return new ServerError("Failed to get Google access token");
            }
            catch
            {
                return new ServerError("Failed to get Google access token");
            }
        };

    public class GoogleAccessTokenResponse
    {
        public string? AccessToken { get; set; }
        public int ExpiresIn { get; set; }
    }
}