namespace AutoScheduler.Data.Google
{
    public interface IGoogleApiClientFactory
    {
        IGoogleClient GetClientForUser(string userId);
    }
}