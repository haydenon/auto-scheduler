using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Data.Google
{
    public interface IGoogleClient
    {
        Task<Either<IError, T>> Get<T>(string uri);
        Task<Either<IError, IList<T>>> GetList<T>(string uri);
    }
}