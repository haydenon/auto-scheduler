using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Common.Models;
using LanguageExt;

namespace AutoScheduler.Data.Google
{
    public class GoogleClient : IGoogleClient
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly JsonSerializerOptions serializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            Converters = { new DateTimeValueConverter() }
        };

        private readonly IGoogleTokenService tokenService;
        private readonly string userId;

        public GoogleClient(string userId, IGoogleTokenService tokenService)
        {
            this.userId = userId;
            this.tokenService = tokenService;
        }

        public async Task<Either<IError, IList<T>>> GetList<T>(string uri)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            return (await RunRequest<ResultsList<T>>(request)).Map(ResultsToList);
        }

        public async Task<Either<IError, T>> Get<T>(string uri)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            return await RunRequest<T>(request);
        }

        private async Task<Either<IError, T>> RunRequest<T>(HttpRequestMessage request)
        {
            var tokenValue = await tokenService.GetAccessToken(userId);
            return await tokenValue.BindAsync<IError, GoogleAccessToken, T>(async token =>
            {
                request.Headers.Add("Authorization", $"Bearer {token.Token}");
                var response = await client.SendAsync(request);
                if (!response.IsSuccessStatusCode)
                {
                    return new ApiError("Error occured querying Google API", response.StatusCode);
                }

                var responseContent = await response.Content.ReadAsStringAsync();

                try
                {
                    var item = JsonSerializer.Deserialize<T>(responseContent, serializerOptions);
                    return item != null ? item : new ApiError("Empty response from Google API", null);
                }
                catch
                {
                    return new ApiError($"Failed to deserialise result of type {typeof(T).FullName}", null);
                }
            });
        }

        private IList<T> ResultsToList<T>(ResultsList<T> list)
            => list.Items ?? new List<T>();
    }
}