using System;

namespace AutoScheduler.Data.Google;

public record GoogleAccessToken(string Token, DateTime ExpiresAt);