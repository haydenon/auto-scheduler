using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Data.Google;

public interface IGoogleTokenService
{
    public Task<Either<IError, GoogleAccessToken>> GetAccessToken(string userId);
}