using System.Collections.Generic;

namespace AutoScheduler.Data.Google
{
    public class ResultsList<T>
    {
        public IList<T>? Items { get; init; }
    }
}