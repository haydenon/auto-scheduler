using System.Collections.Generic;
using System.Threading.Tasks;

namespace AutoScheduler.Data.DatabaseConfiguration
{
    public interface IDatabaseConfigurationRepository
    {
        Task<DatabaseConfigurationData?> GetDatabaseConfigurationForUser(string userId);
        IAsyncEnumerable<DatabaseConfigurationData> GetAllDatabaseConfigurations();
    }
}