using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Data.Cache;
using AutoScheduler.Data.Storage;

namespace AutoScheduler.Data.DatabaseConfiguration
{
    public class DatabaseConfigurationRepository : IDatabaseConfigurationRepository
    {
        private const string CollectionName = "DatabaseConfiguration";
        private readonly IStoreStrategy storeStrategy;
        private readonly IDataCache<DatabaseConfigurationData> cache;

        public DatabaseConfigurationRepository(
            IStoreStrategy storeStrategy,
            IDataCache<DatabaseConfigurationData> cache)
        {
            this.storeStrategy = storeStrategy;
            this.cache = cache;
        }

        public IAsyncEnumerable<DatabaseConfigurationData> GetAllDatabaseConfigurations()
            => storeStrategy.Collection<DatabaseConfigurationData>(CollectionName).GetAllAsync();

        public Task<DatabaseConfigurationData?> GetDatabaseConfigurationForUser(string userId)
            => cache.GetValue(
                    userId,
                    () => storeStrategy.Collection<DatabaseConfigurationData>(CollectionName).GetAsync(userId)
                );
    }
}