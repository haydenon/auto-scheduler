using System.Collections.Generic;
using Google.Cloud.Firestore;

namespace AutoScheduler.Data.DatabaseConfiguration.TaskList
{
    [FirestoreData]
    public class StatusPropertyData
    {
        [FirestoreProperty]
        public string? PropertyId { get; set; }

        [FirestoreProperty]
        public string? DefaultStatusId { get; set; }

        [FirestoreProperty]
        public List<string>? InProgressStatusIds { get; set; }

        [FirestoreProperty]
        public List<string>? FinishedStatusIds { get; set; }
    }
}