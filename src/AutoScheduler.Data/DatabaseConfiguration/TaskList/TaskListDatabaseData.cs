using Google.Cloud.Firestore;

namespace AutoScheduler.Data.DatabaseConfiguration.TaskList
{
    [FirestoreData]
    public class TaskListDatabaseData
    {
        [FirestoreProperty]
        public string? DatabaseId { get; set; }

        [FirestoreProperty]
        public string? ScheduledDatePropertyId { get; set; }

        [FirestoreProperty]
        public StatusPropertyData? StatusProperty { get; set; }
    }
}