using System.Collections.Generic;
using AutoScheduler.Data.DatabaseConfiguration.TaskList;
using Google.Cloud.Firestore;

namespace AutoScheduler.Data.DatabaseConfiguration
{
    [FirestoreData]
    public class DatabaseConfigurationData
    {
        [FirestoreProperty]
        public string? UserId { get; set; }

        [FirestoreProperty]
        public List<TaskListDatabaseData>? TaskListDatabases { get; set; }
    }
}