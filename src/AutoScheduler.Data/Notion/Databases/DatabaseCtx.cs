using System;

namespace AutoScheduler.Data.Notion.Databases
{
    public record DatabaseCtx(Guid? Id);
}