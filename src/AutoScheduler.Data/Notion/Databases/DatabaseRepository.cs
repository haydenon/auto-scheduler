using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Cache;
using AutoScheduler.Data.Google;
using AutoScheduler.Data.Notion.Api;
using AutoScheduler.Data.Notion.Api.Client;
using LanguageExt;

namespace AutoScheduler.Data.Notion.Databases
{
    public class DatabaseRepository : IListRepository<DatabaseData>
    {
        private readonly INotionClient client;
        private readonly IDataCache<IList<DatabaseData>> cache;

        public DatabaseRepository(
            INotionClient client,
            IDataCache<IList<DatabaseData>> cache)
        {
            this.client = client;
            this.cache = cache;
        }

        public Task<Either<IError, IList<DatabaseData>>> FetchAll()
            => cache.GetValue(
                client.UserId,
                () => client.QueryResources<DatabaseData, DatabaseSearchCriteria>(
                    "search",
                    DatabaseSearchCriteria.SearchCriteria
                )
            );
    }

    public class DatabaseRepositoryFactory : IRepositoryFactory<IListRepository<DatabaseData>>
    {
        private readonly IDataCache<IList<DatabaseData>> cache;

        public DatabaseRepositoryFactory(IDataCache<IList<DatabaseData>> cache)
        {
            this.cache = cache;
        }

        public IListRepository<DatabaseData> Create(INotionClient client, IGoogleClient _)
            => new DatabaseRepository(client, cache);
    }
}