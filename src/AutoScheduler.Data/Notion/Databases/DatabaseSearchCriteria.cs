namespace AutoScheduler.Data.Notion.Databases
{
    public record DatabaseFilterCriteria(string Property, string Value);

    public record DatabaseSearchCriteria(DatabaseFilterCriteria Filter)
    {
        public static readonly DatabaseSearchCriteria SearchCriteria =
            new DatabaseSearchCriteria(
                new DatabaseFilterCriteria("object", "database")
            );
    }
}