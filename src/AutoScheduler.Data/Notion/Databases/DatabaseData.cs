using System;
using System.Collections.Generic;
using AutoScheduler.Data.Notion.Common;
using AutoScheduler.Data.Notion.Databases.Properties;

namespace AutoScheduler.Data.Notion.Databases
{
    public class DatabaseData
    {
        public Guid Id { get; init; }
        public List<TextObject>? Title { get; init; }
        public Dictionary<string, PropertyDefinitionData>? Properties { get; init; }
        public string? Url { get; init; }
        public IconData? Icon { get; init; }
    }
}