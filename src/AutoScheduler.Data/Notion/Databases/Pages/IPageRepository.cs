using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Data.Notion.Databases.Pages
{
    public interface IPageRepository
    {
        Task<Either<IError, IList<PageData>>> GetTasksForCriteria(Guid databaseId, IFilterData filterCriteria);
    }
}