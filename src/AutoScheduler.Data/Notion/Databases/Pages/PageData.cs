using System;
using System.Collections.Generic;
using AutoScheduler.Data.Notion.Common;
using AutoScheduler.Data.Notion.Databases.Properties;

namespace AutoScheduler.Data.Notion.Databases.Pages
{
    public class PageData : NotionPageData
    {
        public Guid Id { get; set; }
        public string? Url { get; set; }
        public IconData? Icon { get; set; }
        public Dictionary<string, PropertyValueData>? Properties { get; init; }
    }
}