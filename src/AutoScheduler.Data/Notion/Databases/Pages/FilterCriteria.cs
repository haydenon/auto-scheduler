using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AutoScheduler.Data.Notion.Databases.Pages
{
    public class FilterBody
    {
        public IFilterData? Filter { get; set; }
    }

    public interface IFilterData
    {
    }

    public class PropertyFilterData : IFilterData
    {
        public string? Property { get; set; }

        [JsonExtensionData]
        public Dictionary<string, object>? Critera { get; set; }
    }

    public class CriteriaData
    {
        [JsonExtensionData]
        public Dictionary<string, object>? Values { get; set; }
    }

    public class AndFilterData : IFilterData
    {
        public IList<IFilterData>? And { get; set; }
    }
    public class OrFilterData : IFilterData
    {
        public IList<IFilterData>? Or { get; set; }
    }

    public class CriteriaConverter : JsonConverter<IFilterData>
    {
        public override IFilterData? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }

        public override void Write(Utf8JsonWriter writer, IFilterData value, JsonSerializerOptions options)
        {
            switch (value)
            {
                case PropertyFilterData prop:
                    JsonSerializer.Serialize(writer, prop, options);
                    break;
                case AndFilterData and:
                    JsonSerializer.Serialize(writer, and, options);
                    break;
                case OrFilterData or:
                    JsonSerializer.Serialize(writer, or, options);
                    break;
                default:
                    throw new NotImplementedException($"No implementation for serialising {value.GetType().Name}");
            }
        }

    }
}