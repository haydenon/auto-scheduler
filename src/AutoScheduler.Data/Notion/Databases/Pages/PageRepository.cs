using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Google;
using AutoScheduler.Data.Notion.Api;
using AutoScheduler.Data.Notion.Api.Client;
using LanguageExt;

namespace AutoScheduler.Data.Notion.Databases.Pages
{
    public class PageRepository : IPageRepository
    {
        private readonly INotionClient client;

        public PageRepository(INotionClient client)
        {
            this.client = client;
        }

        public Task<Either<IError, IList<PageData>>> GetTasksForCriteria(Guid databaseId, IFilterData filterCriteria)
            => client.QueryResources<PageData, FilterBody>($"databases/{databaseId}/query", new FilterBody { Filter = filterCriteria });
    }

    public class PageRepositoryFactory : IRepositoryFactory<IPageRepository>
    {
        public IPageRepository Create(INotionClient client, IGoogleClient _)
            => new PageRepository(client);
    }
}