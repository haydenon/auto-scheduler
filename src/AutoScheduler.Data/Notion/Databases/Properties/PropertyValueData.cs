using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using AutoScheduler.Common.Models;
using AutoScheduler.Data.Notion.Common;

namespace AutoScheduler.Data.Notion.Databases.Properties
{
    public class DateValueData
    {
        public IDateTimeValue? Start { get; set; }
        public IDateTimeValue? End { get; set; }
    }

    public class PropertyValueData
    {
        public string? Id { get; set; }
        public string? Type { get; set; }

        #region Property Values

        [JsonPropertyName("multi_select")]
        public List<SelectOptionData>? MultiSelect { get; set; }
        public SelectOptionData? Select { get; set; }

        public DateValueData? Date { get; set; }
        public List<TextObject>? Title { get; set; }

        #endregion
    }
}