using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace AutoScheduler.Data.Notion.Databases.Properties
{
    public class SelectOptionData
    {
        public string? Id { get; set; }
        public string? Name { get; set; }
        public string? Color { get; set; }
    }

    public class SelectOptionList
    {
        public List<SelectOptionData>? Options { get; set; }
    }

    public class PropertyDefinitionData
    {
        public string? Id { get; set; }
        public string? Type { get; set; }
        public string? Name { get; set; }

        [JsonPropertyName("multi_select")]
        public SelectOptionList? MultiSelect { get; set; }
        public SelectOptionList? Select { get; set; }
    }
}