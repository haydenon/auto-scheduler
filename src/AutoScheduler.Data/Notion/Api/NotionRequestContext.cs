namespace AutoScheduler.Data.Notion.Api
{
    public record NotionApiContext(string UserId, string Token, string Version);
}