using System.Collections.Generic;

namespace AutoScheduler.Data.Notion.Api
{
    public class ResultsList<T>
    {
        public IList<T>? Results { get; init; }
    }
}