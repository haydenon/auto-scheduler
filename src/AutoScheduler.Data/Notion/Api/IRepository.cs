using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Data.Notion.Api
{
    public interface IListRepository<TRes>
    {
        Task<Either<IError, IList<TRes>>> FetchAll();
    }
}