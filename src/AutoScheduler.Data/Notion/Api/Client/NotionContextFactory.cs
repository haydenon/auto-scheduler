using System;
using System.Threading.Tasks;
using AutoScheduler.Data.NotionKeys;
using Microsoft.Extensions.Configuration;

namespace AutoScheduler.Data.Notion.Api.Client
{
    public class NotionContextFactory : INotionApiClientFactory
    {
        private const string NotionConfigSection = "Notion";
        private const string NotionApiVersion = "ApiVersion";

        private readonly string version;
        private readonly INotionKeyRepository notionKeyRepository;

        public NotionContextFactory(
            INotionKeyRepository notionKeyRepository,
            IConfiguration configuration)
        {
            this.notionKeyRepository = notionKeyRepository;
            this.version = configuration.GetSection(NotionConfigSection)[NotionApiVersion];
        }

        public Task<INotionClient> GetClientForCurrentUser()
        {
            throw new UnauthorizedAccessException("No current user in scope");
        }

        public async Task<INotionClient> GetClientForUser(string userId)
        {
            var notionKey = await notionKeyRepository.GetNotionKeyForUser(userId);
            if (notionKey?.ApiKey == null)
            {
                throw new UnauthorizedAccessException();
            }

            var context = new NotionApiContext(
                userId,
                notionKey.ApiKey,
                version
            );
            return new NotionClient(context);
        }
    }
}