using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Data.Notion.Api.Client
{
    public interface INotionClient
    {
        string UserId { get; }

        Task<Either<IError, T>> GetResource<T>(string path);

        Task<Either<IError, IList<T>>> GetResources<T>(string path);

        Task<Either<IError, IList<TResp>>> QueryResources<TResp, TCriteria>(string path, TCriteria criteria);
    }
}