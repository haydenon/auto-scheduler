using System.Threading.Tasks;

namespace AutoScheduler.Data.Notion.Api.Client
{
    public interface INotionApiClientFactory
    {
        Task<INotionClient> GetClientForCurrentUser();

        Task<INotionClient> GetClientForUser(string userId);
    }
}