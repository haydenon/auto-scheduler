using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Notion.Api.Client.MockClient.Handlers;
using LanguageExt;
using static LanguageExt.Prelude;

namespace AutoScheduler.Data.Notion.Api.Client.MockClient
{
    public class MockNotionClient : INotionClient
    {
        private static IList<IHandler>? handlers;
        private readonly FileSystemResourceFetcher resourceFetcher;

        public MockNotionClient(string rootDirectory)
        {
            var directory = $"{rootDirectory}/notion";
            resourceFetcher = new FileSystemResourceFetcher(directory);
            var interfaceType = typeof(IHandler);
            if (handlers == null)
            {
                handlers = AppDomain.CurrentDomain.GetAssemblies()
                              .SelectMany(x => x.GetTypes())
                              .Where(x => interfaceType.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                              .Select(x => (IHandler)Activator.CreateInstance(x)!)
                              .ToList();
            }
        }

        public string UserId => "mock_user";

        private async Task<T> Handle<T, TCriteria>(string path, TCriteria? criteria = null)
            where TCriteria : class
        {
            var handler = handlers!.First(h => h.CanHandle(path, criteria));
            return (T)(await handler.Handle(resourceFetcher, path, criteria));
        }

        public async Task<Either<IError, T>> GetResource<T>(string path)
        {
            return await Handle<T, object>(path);
        }

        public async Task<Either<IError, IList<T>>> GetResources<T>(string path)
        {
            var list = await Handle<IList<T>, object>(path)!;
            return Right(list);
        }

        public async Task<Either<IError, IList<TResp>>> QueryResources<TResp, TCriteria>(string path, TCriteria criteria)
        {
            var list = await Handle<IList<TResp>, object>(path, criteria)!;
            return Right(list);
        }
    }
}