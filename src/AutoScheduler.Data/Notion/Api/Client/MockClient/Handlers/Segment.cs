namespace AutoScheduler.Data.Notion.Api.Client.MockClient.Handlers
{
    public class Segment
    {
        private readonly string? value;
        private Segment()
        {
            value = null;
        }

        private Segment(string segment)
        {
            value = segment;
        }

        public static implicit operator Segment(string segment) => new Segment(segment);

        public static Segment Any => new Segment();

        public bool Matches(string segment) => value == null
            ? true
            : value.Equals(segment, System.StringComparison.InvariantCultureIgnoreCase);
    }
}