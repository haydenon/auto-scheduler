using System.Linq;
using System.Threading.Tasks;
using AutoScheduler.Data.Notion.Databases;

namespace AutoScheduler.Data.Notion.Api.Client.MockClient.Handlers
{
    public class DatabaseListHandler : IHandler
    {
        public bool CanHandle(string path, object? criteria = null)
            => path.GetSegments().Matches("search") && object.Equals(criteria, DatabaseSearchCriteria.SearchCriteria);

        public async Task<object> Handle(
            FileSystemResourceFetcher fetcher,
            string path,
            object? criteria = null)
        {
            return await fetcher.GetAllAsync<DatabaseData>().ToListAsync();
        }
    }
}