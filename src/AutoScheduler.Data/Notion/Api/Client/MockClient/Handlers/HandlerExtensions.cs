namespace AutoScheduler.Data.Notion.Api.Client.MockClient.Handlers
{
    public static class HandlerExtensions
    {
        public static string[] GetSegments(this string path)
            => path.Split("/");

        public static bool Matches(this string[] pathSegments, params Segment[] segments)
        {
            if (pathSegments.Length != segments.Length)
            {
                return false;
            }

            for (var i = 0; i < pathSegments.Length; i++)
            {
                if (!segments[i].Matches(pathSegments[i]))
                {
                    return false;
                }
            }

            return true;
        }
    }
}