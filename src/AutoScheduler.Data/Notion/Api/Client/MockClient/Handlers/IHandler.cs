using System.Threading.Tasks;

namespace AutoScheduler.Data.Notion.Api.Client.MockClient.Handlers
{
    public interface IHandler
    {
        bool CanHandle(string path, object? criteria = null);

        Task<object> Handle(FileSystemResourceFetcher fetcher, string path, object? criteria = null);
    }
}