using System.Linq;
using System.Threading.Tasks;
using AutoScheduler.Data.Notion.Databases;
using AutoScheduler.Data.Notion.Databases.Pages;

namespace AutoScheduler.Data.Notion.Api.Client.MockClient.Handlers
{
    public class DatabaseQueryHandler : IHandler
    {
        public bool CanHandle(string path, object? criteria = null)
            => path.GetSegments().Matches("databases", Segment.Any, "query");

        public async Task<object> Handle(FileSystemResourceFetcher fetcher, string path, object? criteria = null)
        {
            var segments = path.GetSegments();
            var parent = Parent.FromType<DatabaseData>(segments[1]);
            return await fetcher.GetAllAsync<PageData>(parent).ToListAsync();
        }
    }
}