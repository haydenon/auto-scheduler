using System.Threading.Tasks;

namespace AutoScheduler.Data.Notion.Api.Client.MockClient
{
    public class MockClientFactory : INotionApiClientFactory
    {
        private readonly string rootDirectory;

        public MockClientFactory(string rootDirectory)
        {
            this.rootDirectory = rootDirectory;
        }

        public Task<INotionClient> GetClientForCurrentUser()
            => GetClientForUser(string.Empty);

        public Task<INotionClient> GetClientForUser(string userId)
            => Task.FromResult<INotionClient>(new MockNotionClient(rootDirectory));
    }
}