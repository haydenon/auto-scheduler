using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace AutoScheduler.Data.Notion.Api.Client.MockClient
{
    public class FileSystemResourceFetcher
    {
        private static readonly JsonSerializerOptions serializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };

        private readonly string notionDataDirectory;

        public FileSystemResourceFetcher(string directory)
        {
            notionDataDirectory = directory;
        }

        private string FilePath<T>(Parent? parent)
            => parent != null
                ? $"{notionDataDirectory}/{parent.GetPath()}/{typeof(T).Name}"
                : $"{notionDataDirectory}/{typeof(T).Name}";

        private string FilePath<T>(Parent? parent, string fileName) => $"{FilePath<T>(parent)}/{fileName}.json";

        public async Task<T?> GetAsync<T>(string documentId, Parent? parent = null)
        {
            var path = FilePath<T>(parent, documentId);
            if (!File.Exists(path))
            {
                return default;
            }

            var contents = await File.ReadAllTextAsync(path);
            return JsonSerializer.Deserialize<T>(contents, serializerOptions);
        }

        public async IAsyncEnumerable<T> GetAllAsync<T>(Parent? parent = null)
        {
            var directory = FilePath<T>(parent);
            var files = Directory.GetFiles(directory);
            foreach (var file in files)
            {
                var contents = await File.ReadAllTextAsync(file);
                yield return JsonSerializer.Deserialize<T>(contents, serializerOptions)!;
            }
        }
    }

    public class Parent
    {
        private readonly Type parentType;
        private readonly string? id;

        private Parent(Type parentType, string? id = null)
        {
            this.parentType = parentType;
            this.id = id;
        }

        public static Parent FromType<T>(string? id = null)
            => new Parent(typeof(T), id);

        public string GetPath()
        {
            var basePath = parentType.Name;
            if (id == null)
            {
                return $"{basePath}/";
            }

            return $"{basePath}/{id}/";
        }
    }
}