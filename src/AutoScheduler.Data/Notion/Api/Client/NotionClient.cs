using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Common.Models;
using AutoScheduler.Data.Notion.Databases.Pages;
using LanguageExt;

namespace AutoScheduler.Data.Notion.Api.Client
{
    public class NotionClient : INotionClient
    {
        private static readonly JsonSerializerOptions serializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            Converters = { new CriteriaConverter(), new DateTimeValueConverter() }
        };

        private static readonly HttpClient client = new HttpClient();
        private const string NotionApiBase = "https://api.notion.com/v1/";

        private readonly NotionApiContext context;

        public NotionClient(NotionApiContext context)
        {
            this.context = context;
        }

        public string UserId => context.UserId;

        public async Task<Either<IError, T>> GetResource<T>(string path)
        {
            var url = $"{NotionApiBase}{path}";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            return await RunRequest<T>(request);
        }

        public async Task<Either<IError, IList<T>>> GetResources<T>(string path)
        {
            var url = $"{NotionApiBase}{path}";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            return (await RunRequest<ResultsList<T>>(request)).Map(ResultsToList);
        }

        public async Task<Either<IError, IList<TResp>>> QueryResources<TResp, TCriteria>(string path, TCriteria criteria)
        {
            var url = $"{NotionApiBase}{path}";
            var body = JsonSerializer.Serialize(criteria, serializerOptions);
            var request = new HttpRequestMessage(HttpMethod.Post, url)
            {
                Content = new StringContent(body, Encoding.UTF8, "application/json")
            };
            return (await RunRequest<ResultsList<TResp>>(request)).Map(ResultsToList);
        }

        private async Task<Either<IError, T>> RunRequest<T>(HttpRequestMessage request)
        {
            request.Headers.Add("Authorization", $"Bearer {context.Token}");
            request.Headers.Add("Notion-Version", context.Version);
            var response = await client.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                return new ApiError("Error occured querying Notion API", response.StatusCode);
            }

            var responseContent = await response.Content.ReadAsStringAsync();

            try
            {
                var item = JsonSerializer.Deserialize<T>(responseContent, serializerOptions);
                return item != null ? item : new ApiError("Empty response from Notion API", null);
            }
            catch
            {
                return new ApiError($"Failed to deserialise result of type {typeof(T).FullName}", null);
            }
        }

        private IList<T> ResultsToList<T>(ResultsList<T> list)
            => list.Results ?? new List<T>();

    }
}