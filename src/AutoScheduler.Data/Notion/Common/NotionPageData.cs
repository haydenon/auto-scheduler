using System;
using System.Text.Json.Serialization;

namespace AutoScheduler.Data.Notion.Common;


public class ParentData
{
    public string? Type { get; set; }

    [JsonPropertyName("database_id")]
    public Guid? DatabaseId { get; set; }

    [JsonPropertyName("page_id")]
    public Guid? PageId { get; set; }

    public bool? Workspace { get; set; }
}

public abstract class NotionPageData
{
    public ParentData? Parent { get; set; }
}