using System.Text.Json.Serialization;

namespace AutoScheduler.Data.Notion.Common
{
    public class TextObject
    {
        [JsonPropertyName("plain_text")]
        public string? PlainText { get; set; }
    }
}