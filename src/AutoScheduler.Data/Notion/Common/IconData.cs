namespace AutoScheduler.Data.Notion.Common
{
    public class IconData
    {
        public string? Emoji { get; set; }
    }
}