using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Data.Cache
{
    public class DataCache<T> : IDataCache<T>
    {
        private readonly IDictionary<string, T> cache = new Dictionary<string, T>();

        public virtual async Task<T?> GetValue(string id, Func<Task<T?>> getter)
        {
            if (cache.ContainsKey(id))
            {
                return cache[id];
            }

            var item = await getter();
            if (item != null)
            {
                cache[id] = item;
            }

            return item;
        }

        public virtual async Task<Either<IError, T>> GetValue(string id, Func<Task<Either<IError, T>>> getter)
        {
            if (cache.ContainsKey(id))
            {
                return cache[id];
            }

            var item = await getter();
            item.Do(val => cache[id] = val);

            return item;
        }

        public void UpdateValue(string id, T value)
        {
            cache[id] = value;
        }
    }
}