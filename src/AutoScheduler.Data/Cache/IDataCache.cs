using System;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Data.Cache
{
    public interface IDataCache<T>
    {
        Task<T?> GetValue(string id, Func<Task<T?>> getter);

        Task<Either<IError, T>> GetValue(string id, Func<Task<Either<IError, T>>> getter);

        void UpdateValue(string id, T value);
    }
}