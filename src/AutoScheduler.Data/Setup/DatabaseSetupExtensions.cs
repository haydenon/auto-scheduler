using System.Collections.Generic;
using AutoScheduler.Data;
using AutoScheduler.Data.Cache;
using AutoScheduler.Data.DatabaseConfiguration;
using AutoScheduler.Data.Google;
using AutoScheduler.Data.Google.Tasks;
using AutoScheduler.Data.GoogleOAuth;
using AutoScheduler.Data.Notion.Api;
using AutoScheduler.Data.Notion.Databases;
using AutoScheduler.Data.Notion.Databases.Pages;
using AutoScheduler.Data.NotionKeys;
using AutoScheduler.Data.Storage;
using AutoScheduler.Data.Tasks;
using AutoScheduler.Data.User;
using Google.Api.Gax.Grpc.GrpcNetClient;
using Google.Cloud.Firestore;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class DatabaseSetupExtensions
    {
        public static void AddDatabase(this IServiceCollection services, IConfiguration configuration, string rootDirectory)
        {
            var useMockData = configuration.GetValue<bool>("UseMockData", false);
            var dbSection = configuration.GetSection("Database");
            if (useMockData)
            {
                services.AddSingleton<IStoreStrategy>(services =>
                {
                    var directory = $"{rootDirectory}/../../test_data";
                    return new FileSystemStrategy(directory);
                });
            }
            else
            {
                var project = dbSection["ProjectId"];
                var db = new FirestoreDbBuilder
                {
                    ProjectId = project,
                    GrpcAdapter = GrpcNetClientAdapter.Default
                }.Build();
                services.AddSingleton(db);
                services.AddSingleton<IStoreStrategy, FirestoreStrategy>();
            }

            services.AddScoped<IDatabaseConfigurationRepository, DatabaseConfigurationRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<INotionKeyRepository, NotionKeyRepository>();
            services.AddScoped<IGoogleOAuthRepository, GoogleOAuthRepository>();
            services.AddScoped<GoogleCalendarTaskSourceRepositoryFactory>();
            services.AddScoped<GoogleTasksTaskSourceRepositoryFactory>();
            services.AddScoped<IRepositoryFactory<ITaskSourceRepository>, MultiTaskSourceRepositoryFactory>();

            services.AddDataCache<DatabaseConfigurationData>();
            services.AddDataCache<NotionKeyData>();
            services.AddDataCache<GoogleOAuthData>();
            services.AddDataCache<IList<TaskListData>>();
            services.AddDataCache<UserDetailsData>();
            services.AddSingleton<IDataCache<GoogleAccessToken>, TokenCache>();
        }

        public static void AddNotionApis(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryFactory<IListRepository<DatabaseData>>, DatabaseRepositoryFactory>();
            services.AddDataCache<IList<DatabaseData>>();

            services.AddScoped<IRepositoryFactory<IPageRepository>, PageRepositoryFactory>();

            services.AddScoped<NotionTaskSourceRepositoryFactory>();
        }

        private static void AddDataCache<T>(this IServiceCollection services)
        {
            services.AddSingleton<IDataCache<T>, DataCache<T>>();
        }
    }
}