using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Data.Cache;
using AutoScheduler.Data.Storage;

namespace AutoScheduler.Data.User;

public class UserRepository : IUserRepository
{
    private const string CollectionName = "UserDetails";
    private readonly IStoreStrategy storeStrategy;
    private readonly IDataCache<UserDetailsData> cache;

    public UserRepository(
        IStoreStrategy storeStrategy,
        IDataCache<UserDetailsData> cache)
    {
        this.storeStrategy = storeStrategy;
        this.cache = cache;
    }

    public Task<UserDetailsData?> GetUser(string userId)
        => cache.GetValue(
                userId,
                () => storeStrategy.Collection<UserDetailsData>(CollectionName).GetAsync(userId)
            );
}