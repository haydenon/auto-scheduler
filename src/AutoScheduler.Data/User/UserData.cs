using Google.Cloud.Firestore;

namespace AutoScheduler.Data.User;

[FirestoreData]
public class UserDetailsData
{
    [FirestoreProperty]
    public string? TimeZone { get; set; }
}