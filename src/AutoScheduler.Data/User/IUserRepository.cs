using System.Threading.Tasks;

namespace AutoScheduler.Data.User;

public interface IUserRepository
{
    Task<UserDetailsData?> GetUser(string userId);
}