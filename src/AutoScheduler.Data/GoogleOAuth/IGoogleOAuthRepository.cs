using System.Threading.Tasks;

namespace AutoScheduler.Data.GoogleOAuth
{
    public interface IGoogleOAuthRepository
    {
        Task<GoogleOAuthData?> GetGoogleOAuthForUser(string userId);

        Task SetRefreshTokenForUser(string userId, string refreshToken);
    }
}