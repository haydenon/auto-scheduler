using Google.Cloud.Firestore;

namespace AutoScheduler.Data.GoogleOAuth
{
    [FirestoreData]
    public class GoogleOAuthData
    {
        [FirestoreProperty]
        public string? RefreshToken { get; set; }
    }
}