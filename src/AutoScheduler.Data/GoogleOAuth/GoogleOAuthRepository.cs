using System.Threading.Tasks;
using AutoScheduler.Data.Cache;
using AutoScheduler.Data.Storage;

namespace AutoScheduler.Data.GoogleOAuth
{
    public class GoogleOAuthRepository : IGoogleOAuthRepository
    {
        private readonly IStoreStrategy storeStrategy;
        private readonly IDataCache<GoogleOAuthData> cache;

        public GoogleOAuthRepository(
            IStoreStrategy storeStrategy,
            IDataCache<GoogleOAuthData> cache)
        {
            this.storeStrategy = storeStrategy;
            this.cache = cache;
        }

        public Task<GoogleOAuthData?> GetGoogleOAuthForUser(string userId)
            => cache.GetValue(
                userId,
                    () => storeStrategy.Collection<GoogleOAuthData>("GoogleOAuth").GetAsync(userId)
                );

        public async Task SetRefreshTokenForUser(string userId, string refreshToken)
        {
            var store = storeStrategy.Collection<GoogleOAuthData>("GoogleOAuth");
            var oauthData = await cache.GetValue(
                userId,
                () => store.GetAsync(userId)
            );

            oauthData ??= new GoogleOAuthData();
            oauthData.RefreshToken = refreshToken;

            await store.SetAsync(userId, oauthData);
            cache.UpdateValue(userId, oauthData);
        }
    }
}