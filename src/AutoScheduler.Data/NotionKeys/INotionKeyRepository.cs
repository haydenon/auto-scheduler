using System.Threading.Tasks;

namespace AutoScheduler.Data.NotionKeys
{
    public interface INotionKeyRepository
    {
        Task<NotionKeyData?> GetNotionKeyForUser(string userId);
    }
}