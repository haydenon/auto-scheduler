using System.Threading.Tasks;
using AutoScheduler.Data.Cache;
using AutoScheduler.Data.Storage;

namespace AutoScheduler.Data.NotionKeys
{
    public class NotionKeyRepository : INotionKeyRepository
    {
        private readonly IStoreStrategy storeStrategy;
        private readonly IDataCache<NotionKeyData> cache;

        public NotionKeyRepository(
            IStoreStrategy storeStrategy,
            IDataCache<NotionKeyData> cache)
        {
            this.storeStrategy = storeStrategy;
            this.cache = cache;
        }

        public Task<NotionKeyData?> GetNotionKeyForUser(string userId)
            => cache.GetValue(
                userId,
                    () => storeStrategy.Collection<NotionKeyData>("NotionKeys").GetAsync(userId)
                );
    }
}