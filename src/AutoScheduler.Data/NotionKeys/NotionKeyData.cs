using Google.Cloud.Firestore;

namespace AutoScheduler.Data.NotionKeys
{
    [FirestoreData]
    public class NotionKeyData
    {
        [FirestoreProperty]
        public string? ApiKey { get; set; }
    }
}