using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Common.Models;
using AutoScheduler.Data.Cache;
using AutoScheduler.Data.Google;
using AutoScheduler.Data.Google.Tasks;
using AutoScheduler.Data.Notion.Api.Client;
using AutoScheduler.Data.User;
using LanguageExt;
using Microsoft.Extensions.Logging;
using TimeZoneConverter;
using GoogleTaskData = AutoScheduler.Data.Google.Tasks.TaskData;
using SchedulerTaskData = AutoScheduler.Data.Tasks.TaskData;

namespace AutoScheduler.Data.Tasks
{
    public class GoogleTasksTaskSourceRepository : ITaskSourceRepository
    {
        private const string BaseUri = "https://www.googleapis.com/tasks/v1";
        private static readonly string TaskListUri = $"{BaseUri}/users/@me/lists";

        private readonly IGoogleClient googleClient;
        private readonly ILogger<GoogleTasksTaskSourceRepository> logger;
        private readonly IDataCache<IList<TaskListData>> taskListCache;
        private readonly IUserRepository userRepository;

        public GoogleTasksTaskSourceRepository(
            IGoogleClient googleClient,
            ILogger<GoogleTasksTaskSourceRepository> logger,
            IDataCache<IList<TaskListData>> taskListCache,
            IUserRepository userRepository)
        {
            this.googleClient = googleClient;
            this.logger = logger;
            this.taskListCache = taskListCache;
            this.userRepository = userRepository;
        }

        public async Task<Either<IError, IList<SchedulerTaskData>>> GetTasksForUser(string userId, TaskDataFilter filter)
        {
            var user = await userRepository.GetUser(userId);
            var timeZone = user?.TimeZone;
            if (timeZone == null)
            {
                return new List<SchedulerTaskData>();
            }
            var maxTimeValue = filter.MaxTime;
            var maxTime = maxTimeValue.HasValue
                ? GetMaxTime(maxTimeValue.Value, TZConvert.GetTimeZoneInfo(timeZone))
                : null;
            var taskListIds = await GetTaskListIds(userId);
            return await taskListIds.BindAsync(async ids =>
            {
                var tasks = await Task.WhenAll(ids.Select(id => GetTasksForTaskList(id, maxTime)));
                var taskList = tasks.Reduce((acc, next) => acc.Bind(
                    l1 => next.Map(l2 => (IList<GoogleTaskData>)l1.Append(l2).ToList())
                ));
                return taskList.Map(t => (IList<SchedulerTaskData>)t.Select(MapToTaskData).ToList());
            });
        }

        private string GetMaxTime(DateTimeOffset dateTime, TimeZoneInfo timeZone)
        {
            var localTime = TimeZoneInfo.ConvertTimeFromUtc(dateTime.UtcDateTime, timeZone);

            // Tasks API represents the date using UTC, even though it's just the date component
            return DateTime.SpecifyKind(localTime.Date.AddDays(1), DateTimeKind.Utc).ToString("o");
        }

        private async Task<Either<IError, IList<GoogleTaskData>>> GetTasksForTaskList(string taskListId, string? maxTime)
        {
            var uri = $"{TaskListTasksUri(taskListId)}{GetParams(maxTime)}";
            return await googleClient.GetList<GoogleTaskData>(uri);
        }

        private async Task<Either<IError, IEnumerable<string>>> GetTaskListIds(string userId)
        {
            var taskLists = await taskListCache.GetValue(
                userId,
                () => googleClient.GetList<TaskListData>(TaskListUri)
            );

            return taskLists.Map(taskLists => taskLists.Select(tl => tl.Id!));
        }

        private SchedulerTaskData MapToTaskData(GoogleTaskData taskData)
        {
            var start = taskData.Due.HasValue
                ? new DateOnlyValue(
                    new DateOnly(
                        taskData.Due.Value.Date.Year,
                        taskData.Due.Value.Date.Month,
                        taskData.Due.Value.Date.Day
                    )
                )
                : (IDateTimeValue)new DateOnlyValue(DateOnly.MinValue);
            var end = start;
            return new SchedulerTaskData
            {
                TaskId = taskData.Id ?? string.Empty,
                Source = "google_tasks",
                Title = taskData.Title ?? string.Empty,
                Start = start,
                End = end,
                IsRecurring = false
            };
        }

        private string GetParams(string? max)
        {
            var args = new List<string>{
                "showCompleted=false",
                "showDeleted=false",
                "showHidden=false",
            };
            if (max != null)
            {
                args.Add($"dueMax={max}");
            }

            return args.Any()
                ? $"?{string.Join('&', args)}"
                : string.Empty;
        }

        private string TaskListTasksUri(string listId)
            => $"{BaseUri}/lists/{listId}/tasks";
    }

    public class GoogleTasksTaskSourceRepositoryFactory : IRepositoryFactory<GoogleTasksTaskSourceRepository>
    {
        private readonly ILogger<GoogleTasksTaskSourceRepository> logger;
        private readonly IDataCache<IList<TaskListData>> taskListCache;
        private readonly IUserRepository userRepository;

        public GoogleTasksTaskSourceRepositoryFactory(
            ILogger<GoogleTasksTaskSourceRepository> logger,
            IDataCache<IList<TaskListData>> taskListCache,
            IUserRepository userRepository)
        {
            this.logger = logger;
            this.taskListCache = taskListCache;
            this.userRepository = userRepository;
        }

        public GoogleTasksTaskSourceRepository Create(INotionClient _, IGoogleClient client)
            => new GoogleTasksTaskSourceRepository(client, logger, taskListCache, userRepository);
    }
}