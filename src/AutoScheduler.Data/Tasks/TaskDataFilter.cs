using System;

namespace AutoScheduler.Data.Tasks;

public record TaskDataFilter(
    DateTimeOffset? MinTime,
    DateTimeOffset? MaxTime
);