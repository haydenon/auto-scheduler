using System.Collections.Generic;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using LanguageExt;

namespace AutoScheduler.Data.Tasks
{
    public interface ITaskSourceRepository
    {
        Task<Either<IError, IList<TaskData>>> GetTasksForUser(string userId, TaskDataFilter filter);
    }
}