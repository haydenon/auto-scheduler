using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.Google;
using AutoScheduler.Data.Notion.Api;
using AutoScheduler.Data.Notion.Api.Client;
using LanguageExt;
using Microsoft.Extensions.DependencyInjection;

namespace AutoScheduler.Data.Tasks
{

    public class MultiTaskSourceRepository : ITaskSourceRepository
    {
        private readonly ICollection<ITaskSourceRepository> childRepositories;

        public MultiTaskSourceRepository(ICollection<ITaskSourceRepository> childRepositories)
        {
            this.childRepositories = childRepositories;
        }

        public async Task<Either<IError, IList<TaskData>>> GetTasksForUser(string userId, TaskDataFilter filter)
        {
            var tasksData = await Task.WhenAll(childRepositories.Select(r => r.GetTasksForUser(userId, filter)));
            return tasksData.Reduce((acc, next) => acc.Bind(
                l1 => next.Map(l2 => (IList<TaskData>)l1.Append(l2).ToList())
            ));
        }
    }

    public class MultiTaskSourceRepositoryFactory : IRepositoryFactory<ITaskSourceRepository>
    {
        private readonly IServiceProvider serviceProvider;

        public MultiTaskSourceRepositoryFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public ITaskSourceRepository Create(INotionClient notionClient, IGoogleClient googleClient)
            => new MultiTaskSourceRepository(GetChildren(notionClient, googleClient));

        private ICollection<ITaskSourceRepository> GetChildren(INotionClient client, IGoogleClient googleClient)
            => new List<ITaskSourceRepository>
            {
                serviceProvider.GetRequiredService<NotionTaskSourceRepositoryFactory>().Create(client, googleClient),
                serviceProvider.GetRequiredService<GoogleCalendarTaskSourceRepositoryFactory>().Create(client, googleClient),
                serviceProvider.GetRequiredService<GoogleTasksTaskSourceRepositoryFactory>().Create(client, googleClient)
            };
    }
}