using System;
using AutoScheduler.Common.Models;

namespace AutoScheduler.Data.Tasks
{
    public class TaskData
    {
        public string? TaskId;
        public string? Source;
        public string? Title;
        public IDateTimeValue? Start;
        public IDateTimeValue? End;
        public bool IsRecurring;
    }
}