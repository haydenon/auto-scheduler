using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Data.DatabaseConfiguration;
using AutoScheduler.Data.Google;
using AutoScheduler.Data.Notion.Api;
using AutoScheduler.Data.Notion.Api.Client;
using AutoScheduler.Data.Notion.Databases;
using AutoScheduler.Data.Notion.Databases.Pages;
using AutoScheduler.Data.Notion.Databases.Properties;
using LanguageExt;
using Microsoft.Extensions.DependencyInjection;

namespace AutoScheduler.Data.Tasks
{
    public class NotionTaskSourceRepository : ITaskSourceRepository
    {
        private readonly IPageRepository pageRepository;
        private readonly IListRepository<DatabaseData> databaseRepository;
        private readonly IDatabaseConfigurationRepository databaseConfigurationRepository;
        private readonly INotionClient client;

        public NotionTaskSourceRepository(
            INotionClient client,
            IPageRepository pageRepository,
            IDatabaseConfigurationRepository databaseConfigurationRepository,
            IListRepository<DatabaseData> databaseRepository)
        {
            this.client = client;
            this.pageRepository = pageRepository;
            this.databaseConfigurationRepository = databaseConfigurationRepository;
            this.databaseRepository = databaseRepository;
        }

        public async Task<Either<IError, IList<TaskData>>> GetTasksForUser(string userId, TaskDataFilter filter)
        {
            var databaseConfigurationTask = databaseConfigurationRepository.GetDatabaseConfigurationForUser(userId);
            var allDatabases = await databaseRepository.FetchAll();
            var databaseConfiguration = await databaseConfigurationTask;
            if (databaseConfiguration?.TaskListDatabases == null)
            {
                return new TaskData[0];
            }

            var databaseIds = databaseConfiguration.TaskListDatabases.Select(d => d.DatabaseId).ToList();
            var databases = allDatabases.Match(dbs => dbs.Where(db => databaseIds.Contains(db.Id.ToString())), _ => throw new InvalidOperationException());
            var allPages = await Task.WhenAll(
                databases.Select(
                    db => pageRepository.GetTasksForCriteria(
                        db.Id,
                        MapFilter(filter, databaseConfiguration, db)
                    )
                )
            );
            var pages = allPages.Reduce((acc, next) => acc.Bind(
                l1 => next.Map(l2 => (IList<PageData>)l1.Append(l2).ToList())
            ));

            return pages.Map(ps => (IList<TaskData>)ps.Select(pd => MapFromPageData(pd, databaseConfiguration)).ToList());
        }

        private static string? GetTitleFromProperties(IEnumerable<PropertyValueData> properties)
        {
            var titleProp = properties.FirstOrDefault(p => p.Title != null);
            if (titleProp?.Title == null)
            {
                return null;
            }

            return string.Join("", titleProp.Title.Select(t => t.PlainText ?? string.Empty) ?? new string[] { });
        }

        private TaskData? MapFromPageData(PageData data, DatabaseConfigurationData configuration)
        {
            var databaseId = data.Parent?.DatabaseId!;
            var databaseConfig = configuration.TaskListDatabases?.FirstOrDefault(db => db.DatabaseId == databaseId.ToString());
            if (databaseConfig == null)
            {
                return null;
            }

            var scheduledDatePropertyId = databaseConfig.ScheduledDatePropertyId;
            if (data.Properties == null)
            {
                return null;
            }

            var properties = data.Properties.Values;
            var dateProperty = properties.FirstOrDefault(p => p.Id == scheduledDatePropertyId);
            if (dateProperty?.Date?.Start == null)
            {
                return null;
            }

            var start = dateProperty.Date.Start;
            var end = dateProperty.Date.End ?? start;
            var title = GetTitleFromProperties(properties);

            return new TaskData
            {
                TaskId = data.Id.ToString(),
                Source = "notion",
                Title = title,
                Start = start,
                End = end,
                IsRecurring = false
            };
        }

        private IFilterData MapFilter(TaskDataFilter filter, DatabaseConfigurationData config, DatabaseData database)
        {
            var configDb = config.TaskListDatabases?.First(db => db.DatabaseId == database.Id.ToString())!;
            var dateProperty = database.Properties?.First(p => p.Value.Id == configDb.ScheduledDatePropertyId).Value!;
            var criteria = new List<IFilterData>();
            if (filter.MinTime != null)
            {
                var minTime = filter.MinTime.Value;
                criteria.Add(new PropertyFilterData
                {
                    Property = dateProperty.Name,
                    Critera = new Dictionary<string, object>
                    {
                        {   "date",
                            new CriteriaData
                            {
                                Values = new Dictionary<string, object> { { "on_or_after", minTime.ToString("u") } }
                            }
                        }
                    }
                });
            }
            if (filter.MaxTime != null)
            {
                var maxTime = filter.MaxTime.Value;
                criteria.Add(new PropertyFilterData
                {
                    Property = dateProperty.Name,
                    Critera = new Dictionary<string, object>
                    {
                        {   "date",
                            new CriteriaData
                            {
                                Values = new Dictionary<string, object> { { "on_or_before", maxTime.ToString("u") } }
                            }
                        }
                    }
                });
            }

            return new AndFilterData
            {
                And = criteria
            };
        }
    }

    public class NotionTaskSourceRepositoryFactory : IRepositoryFactory<ITaskSourceRepository>
    {
        private readonly IServiceProvider serviceProvider;

        public NotionTaskSourceRepositoryFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public ITaskSourceRepository Create(INotionClient client, IGoogleClient _)
            => new NotionTaskSourceRepository(
                client,
                serviceProvider.GetRequiredService<IRepositoryFactory<IPageRepository>>().Create(client, _),
                serviceProvider.GetRequiredService<IDatabaseConfigurationRepository>(),
                serviceProvider.GetRequiredService<IRepositoryFactory<IListRepository<DatabaseData>>>().Create(client, _)
            );
    }
}