using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoScheduler.Common.Errors;
using AutoScheduler.Common.Models;
using AutoScheduler.Data.Google;
using AutoScheduler.Data.Google.Events;
using AutoScheduler.Data.Notion.Api.Client;
using LanguageExt;
using Microsoft.Extensions.Logging;
using static AutoScheduler.Prelude;

namespace AutoScheduler.Data.Tasks
{
    public class GoogleCalendarTaskSourceRepository : ITaskSourceRepository
    {
        private const string BaseUri = "https://www.googleapis.com/calendar/v3";
        private static readonly string EventsUri = $"{BaseUri}/calendars/primary/events";

        private readonly IGoogleClient googleClient;
        private readonly ILogger<GoogleCalendarTaskSourceRepository> logger;

        public GoogleCalendarTaskSourceRepository(
            IGoogleClient googleClient,
            ILogger<GoogleCalendarTaskSourceRepository> logger)
        {
            this.googleClient = googleClient;
            this.logger = logger;
        }

        public async Task<Either<IError, IList<TaskData>>> GetTasksForUser(string userId, TaskDataFilter filter)
        {
            var minTime = filter.MinTime?.UtcDateTime.ToString("o");
            var maxTime = filter.MaxTime?.UtcDateTime.ToString("o");
            var uri = $"{EventsUri}{GetStartEndParams(minTime, maxTime)}";

            var events = await googleClient.GetList<EventData>(uri).BindAsync(e => GetInstancesForRepeating(e, minTime, maxTime));
            return events.Map(e => (IList<TaskData>)e.Select(MapToTaskData).ToList());
        }

        private async Task<Either<IError, IList<EventData>>> GetInstancesForRepeating(IList<EventData> events, string? minTime, string? maxTime)
        {
            var recurring = events.Where(IsRecurring);
            var notRecurring = events.Where(Not<EventData>(IsRecurring));

            var results = await Task.WhenAll(recurring.Select(e => GetInstancesForEvent(e, minTime, maxTime)));
            var successful = results.Rights();
            var errors = results.Lefts();
            foreach (var error in errors)
            {
                logger.LogError("Error occurred: {Error}", new { error.Message });
            }
            if (successful.Any() || notRecurring.Any() || !errors.Any())
            {
                return notRecurring.Append(successful.SelectMany(instances => instances)).ToList();
            }
            else
            {
                return new AggregateError(errors);
            }
        }

        private async Task<Either<IError, IList<EventData>>> GetInstancesForEvent(EventData eventData, string? minTime, string? maxTime)
        {
            var uri = $"{EventInstanceUri(eventData.Id!)}{GetStartEndParams(minTime, maxTime)}";
            var instances = await googleClient.GetList<EventData>(uri);
            return instances.Map(e => (IList<EventData>)e.ToList());
        }

        private static bool IsRecurring(EventData e) => e.Recurrence != null;

        private TaskData MapToTaskData(EventData eventData)
        {
            var start = eventData.Start?.DateTime ?? eventData.Start?.Date ?? (IDateTimeValue)new DateOnlyValue(DateOnly.MinValue);
            var end = eventData.End?.DateTime ?? eventData.End?.Date ?? start;
            return new TaskData
            {
                TaskId = eventData.Id ?? string.Empty,
                Source = "google_calendar",
                Title = eventData.Summary ?? string.Empty,
                Start = start,
                End = end,
                IsRecurring = IsRecurring(eventData)
            };
        }

        private string GetStartEndParams(string? min, string? max)
        {
            var args = new List<string>();
            if (min != null)
            {
                args.Add($"timeMin={min}");
            }
            if (max != null)
            {
                args.Add($"timeMax={max}");
            }

            return args.Any()
                ? $"?{string.Join('&', args)}"
                : string.Empty;
        }

        private string EventInstanceUri(string eventId)
            => $"{EventsUri}/{eventId}/instances";
    }

    public class GoogleCalendarTaskSourceRepositoryFactory : IRepositoryFactory<GoogleCalendarTaskSourceRepository>
    {
        private readonly ILogger<GoogleCalendarTaskSourceRepository> logger;

        public GoogleCalendarTaskSourceRepositoryFactory(ILogger<GoogleCalendarTaskSourceRepository> logger)
        {
            this.logger = logger;
        }

        public GoogleCalendarTaskSourceRepository Create(INotionClient _, IGoogleClient client)
            => new GoogleCalendarTaskSourceRepository(client, logger);
    }
}