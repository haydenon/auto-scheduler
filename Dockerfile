FROM mcr.microsoft.com/dotnet/sdk:6.0@sha256:9bae313dfa1699e935c963508917de903a63b055c3ddc9fc1b5955533e049b26 AS build
ARG PROJECT

WORKDIR /app

# Copy everything else and build
COPY ./AutoScheduler.sln ./
COPY ./src ./src
RUN cd ${PROJECT} && \
    dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine@sha256:f37ba7086c2ee0a71db3f7a51130b81602767611c190365df8c5c370d449346a AS bin
ARG PROJECT
ARG ENTRY_DLL
ENV ENTRY_DLL=${ENTRY_DLL}

RUN apk add --no-cache tzdata

WORKDIR /app
COPY --from=build /app/${PROJECT}/out .
COPY ./lib/libgrpc_csharp_ext.so /app/libgrpc_csharp_ext.x64.so
RUN printf "#!/bin/sh\ndotnet ${ENTRY_DLL}" > start.sh && \
    chmod +x start.sh
ENTRYPOINT ["/bin/sh", "./start.sh"]
